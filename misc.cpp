/***************************************************************************
 *   Copyright (C) 2007-2010 by Miguel Chavez Gamboa                       *
 *   miguel@lemonpos.org                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/
#include "misc.h"
#include <QCoreApplication>
#include <QDebug>

QStringList Misc::stringToParagraph(const QString &str, const QFontMetrics &fm, const double &maxL)
{
    QStringList strList;
    QString strCopy = str;
    double strW = fm.size(Qt::TextExpandTabs | Qt::TextDontClip, str).width();
    double realTrozos = strW / maxL;
    int trozos   = realTrozos;
    double diff = (realTrozos - trozos);
    if (diff > 0.25 && trozos > 0) trozos += 1;
    int tamTrozo = 0;
    if (trozos > 0) {
        tamTrozo = (str.length()/trozos);
    } else {
        tamTrozo = str.length();
    }

    QStringList otherList;
    for (int x = 1; x <= trozos; x++) {
        //we repeat for each trozo
        if (x*(tamTrozo-1) < strCopy.length())
            strCopy.insert(x*(tamTrozo-1), "|  "); //create a section
    }
    otherList = strCopy.split("|");
    if (!otherList.isEmpty()) strList << otherList;
    if (trozos < 1) strList << str;
    //qDebug()<<"rm : Trozos:"<<trozos<<" tamTrozo:"<<tamTrozo<<" realTrozos:"<<QString::number(realTrozos,'f', 2)<<" maxL:"<<maxL<<" str.width in pixels:"<<fm.size(Qt::TextExpandTabs | Qt::TextDontClip, str).width()<<" diff:"<<diff;

    return strList;
}

QStringList Misc::stringToParagraph(const QString &str, const int &maxChars)
{
    QStringList strList;
    QString strCopy = str;
    double strLen = str.length();
    double realTrozos = strLen / maxChars;
    int trozos   = realTrozos;
    double diff = (realTrozos - trozos);
    if (diff > 0 && trozos > 0) trozos += 1;
    int tamTrozo = 0;
    if (trozos > 0) {
        tamTrozo = (str.length()/trozos);
    } else {
        tamTrozo = str.length();
    }

    QStringList otherList;
    for (int x = 1; x <= trozos; x++) {
        //we repeat for each trozo
        if (x*(tamTrozo-1) < strCopy.length())
            strCopy.insert(x*(tamTrozo-1), "|"); //create a section
    }
    otherList = strCopy.split("|");
    if (!otherList.isEmpty()) strList << otherList;
    if (trozos < 1) strList << str;
    //qDebug()<<"rm : Trozos:"<<trozos<<" tamTrozo:"<<tamTrozo<<" realTrozos:"<<QString::number(realTrozos,'f', 2)<<"Str Length: "<<str.length()<<" maxChars:"<<maxChars<<" diff:"<<diff;

    return strList;
}

//share/icons
QString Misc::getAppIconPath()
{
    /** @note: This is not the best, but the first try. Using :
        QCoreApplication::applicationDirPath ()
        Removing "bin/" and adding  'share/apps/lemonCourier/'
        Normally the install path for the binaries is '/usr/bin' or '/usr/local/bin', so we use this information.
      **/

    QString result = QCoreApplication::applicationDirPath();
    qDebug()<<" App path:"<<result;

    //remove the bin part.
    if ( result.endsWith("/") )
        result = result.remove("bin/");
    else
        result = result.remove("bin");

    //appending the 'share/icons/'
    result = result.append("share/icons/");
    qDebug()<<" App path:"<<result;


    return result;
}

QString Misc::getAppDataPath()
{
    /** @note: This is not the best, but the first try. Using :
        QCoreApplication::applicationDirPath ()
        Removing "bin/" and adding  'share/apps/lemonCourier/'
        Normally the install path for the binaries is '/usr/bin' or '/usr/local/bin', so we use this information.
      **/

    QString result = QCoreApplication::applicationDirPath();
    qDebug()<<" App path:"<<result;

    //remove the bin part.
    if ( result.endsWith("/") )
        result = result.remove("bin/");
    else
        result = result.remove("bin");

    //appending the 'share/apps/lemonCourier/'
    result = result.append("share/apps/lemonCourier/");
    qDebug()<<" App path:"<<result;


    return result;
}

QString Misc::getAppInstallPath()
{
    /** @note: This is not the best, but the first try. Using :
        QCoreApplication::applicationDirPath ()
        Removing "bin/" and adding  'share/apps/lemonCourier/'
        Normally the install path for the binaries is '/usr/bin' or '/usr/local/bin', so we use this information.
      **/

    QString result = QCoreApplication::applicationDirPath();

    //remove the bin part.
    if ( result.endsWith("/") )
        result = result.remove("bin/");
    else
        result = result.remove("bin");

    return result;
}
