/**************************************************************************
*   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
*   miguel@lemonpos.org                                                   *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
***************************************************************************/

#define QT_USE_FAST_CONCATENATION
#define QT_USE_FAST_OPERATOR_PLUS

#include "print-cups.h"
#include "../misc.h"

#include <QString>
#include <QFont>
#include <QtGui/QPrinter>
#include <QPainter>
#include <QLocale>
#include <QDebug>


bool PrintCUPS::printSubOrder(const LCSubOrderInfo &ptInfo, /*const qulonglong &provid, const Azahar &myDb,*/ QPrinter &printer)
{
  bool result = false;
  QFont header = QFont("Impact", 38);
  const int Margin = 20;
  int yPos        = 0;
  QPainter painter;
  painter.begin( &printer );
  QFontMetrics fm = painter.fontMetrics();
  
  //NOTE from Qt for the drawText: The y-position is used as the baseline of the font
  
  QFont tmpFont = QFont("Bitstream Vera Sans", 18);
  QPen normalPen = painter.pen();
  QString text;
  QSize textWidth;

  //Order No.
  tmpFont = QFont("Bitstream Vera Sans", 22);
  tmpFont.setItalic(false);
  tmpFont.setBold(true);
  painter.setFont(tmpFont);
  fm = painter.fontMetrics();
  text = "Order No. " + QString::number(ptInfo.orderid); //tr("Order No. ") FIXME: TRANSLATIONS!
  textWidth = fm.size(Qt::TextExpandTabs | Qt::TextDontClip, text);
  painter.drawText((printer.width()/2)-(textWidth.width()/2), Margin + yPos +textWidth.height(), text);
  yPos = yPos + fm.lineSpacing();

  //Date
  tmpFont = QFont("Bitstream Vera Sans", 16);
  tmpFont.setBold(false);
  painter.setFont(tmpFont);
  fm = painter.fontMetrics();
  text = QDateTime::currentDateTime().toString("ddd MMM d yyyy, h:mm ap");
  textWidth = fm.size(Qt::TextExpandTabs | Qt::TextDontClip, text);
  painter.drawText((printer.width()/2)-(textWidth.width()/2), Margin + yPos +textWidth.height(), text);
  yPos = yPos + 2*fm.lineSpacing();

  // Header line
  painter.setPen(QPen(Qt::gray, 5, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
  painter.drawLine(Margin, Margin + yPos+10, printer.width()-Margin, Margin+yPos+10);

  //change font again
  tmpFont = QFont("Bitstream Vera Sans", 17);
  tmpFont.setItalic(false);
  tmpFont.setBold(false);
  painter.setPen(normalPen);
  fm = painter.fontMetrics();
  yPos = yPos + 2*fm.lineSpacing();
  // End of Header Information.

  //get each item. The items are only for the specified sub order.
  foreach (LCPrintOrderItems itm, ptInfo.items) {
      text = "   "+QString::number(itm.qty)+" x "+itm.name;
      tmpFont.setBold(false);
      painter.setFont(tmpFont);
      fm = painter.fontMetrics();
      painter.drawText(Margin, Margin+yPos, text);
      yPos = yPos + fm.lineSpacing();
  }//each item for each suborder

  //Print The sub order COMMENTS
  double maxLen = (printer.width())-100;
  QStringList tmpStr = Misc::stringToParagraph(ptInfo.comments, fm, maxLen );
  yPos = yPos + fm.lineSpacing();
  foreach(QString str, tmpStr) {
      painter.drawText(Margin, Margin+yPos, str);
      yPos = yPos + fm.lineSpacing();
  }

  painter.setPen(QPen(Qt::darkGray, 1, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
  painter.drawLine(Margin, Margin + yPos - 8, printer.width()-Margin, Margin + yPos - 8);
  
  result = painter.end();// this makes the print job start
  
  return result;
}

