/**************************************************************************
*   Copyright (C) 2007-2009 by Miguel Chavez Gamboa                       *
*   miguel@lemonpos.org                                                   *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
***************************************************************************/
#ifndef AZAHAR_H
#define AZAHAR_H

#define QT_USE_FAST_CONCATENATION
#define QT_USE_FAST_OPERATOR_PLUS

#include <QObject>
#include <QtSql>
#include <QObject>
#include <QHash>
#include "structs.h" /** @note: before cmake-fy, there was ../structs.h which does not exists! **/

/**
 * This class makes all database access.
 *
 * @short Database access class
 * @author Miguel Chavez Gamboa <miguel@lemonpos.org>
 * @version 0.1
 */
class Azahar : public QObject
{
    Q_OBJECT
private:
    QSqlDatabase db;
    QString errorStr;
    void setError(QString err);
public:
    explicit Azahar(QObject *parent = 0);
    bool isConnected();
    QString lastError();
    void setDatabase(const QSqlDatabase& database);

    // Kitchen Order

    ///gets the order info needed to print the kitchen order.
    LCPrintOrderInfo getPrintOrderInfo(qulonglong oid);
    LCSubOrderInfo getPrintSubOrderInfo(qulonglong soid);


    //Clients

    ///get client id for specified orderid
    qulonglong   getClientIdForOrder(qulonglong order);
    LCClientInfo getClientInfo(qulonglong clientid);

    //Providers

    //get provider info.
    QString getProviderName(qulonglong id);
    QString getProviderPosition(qulonglong id);

    //Couriers
    LCCourierInfo getCourierInfo(qulonglong id);

    //Shipments

    ///get Comments for the specified sub order
    QString getCommentsForShipment(qulonglong soid);
    ///get each suborder for the specified order
    QList<LCSubOrderInfo> getSubOrders(qulonglong orderid);
    qulonglong getSubOrderOrderId(const qulonglong &soid);

    //Orders

    ///get Order Items for the specified sub order
    QList<LCPrintOrderItems> getPrintOrderItems(qulonglong soid);
    bool    orderExists(const qulonglong &oid);
    int     getOrderStatus(const qulonglong &oid);
    bool    setEachSubOrderStatus(const qulonglong &orderid, const int &status);
    bool    setSubOrderETA(const qulonglong &id, const int &minutes);
    bool    setOrderETA(const qulonglong &id, const int &minutes);
    bool    setOrderStatus(const qulonglong &id, const int &status, const qulonglong &courierid, const bool &updateSubOrders=true);
    bool    setOrderStatus(const qulonglong &id, const int &status);
    bool    setSubOrderStatus(const qulonglong &id, const int &status);
    bool    setPing(const qulonglong &order);
    void    updateOrderStatus(const qulonglong &suborder); //this is for updating the order status of which the suborder is part of.
    int     getOrderPickedItemsCount(const qulonglong &order);
    int     getOrderItemsCount(const qulonglong &order);
    QDateTime getLastPing(const qulonglong &order);
    QList<qulonglong> getOrphanOrders(const qulonglong &courierId=0);
    int    interruptedOrders( const qulonglong &courierId=0 );

    //Cities and States
    QString getCityName(qulonglong id);
    QString getStateName(qulonglong id);

    LCUserInfo getUserInfo(const qulonglong &id);

signals:

public slots:

};

#endif // AZAHAR_H
