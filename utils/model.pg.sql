BEGIN;
CREATE TABLE "categories" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(765) NOT NULL
)
;
CREATE TABLE "states" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(255) NOT NULL
)
;
CREATE TABLE "cities" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(255) NOT NULL,
    "state_id" integer NOT NULL REFERENCES "states" ("id") DEFERRABLE INITIALLY DEFERRED
)
;
CREATE TABLE "provider_categories" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(250) NOT NULL
)
;
CREATE TABLE "providers" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(765) NOT NULL,
    "email" varchar(75) NOT NULL,
    "phone" varchar(20) NOT NULL,
    "category_id" integer NOT NULL REFERENCES "provider_categories" ("id") DEFERRABLE INITIALLY DEFERRED
)
;
CREATE TABLE "clients" (
    "id" serial NOT NULL PRIMARY KEY,
    "firstname" varchar(255) NOT NULL,
    "lastname" varchar(255) NOT NULL,
    "email" varchar(75) NOT NULL,
    "password" varchar(765) NOT NULL,
    "phone" varchar(20) NOT NULL,
    "addr1" varchar(765) NOT NULL,
    "addr2" varchar(765) NOT NULL,
    "city_id" integer NOT NULL REFERENCES "cities" ("id") DEFERRABLE INITIALLY DEFERRED,
    "state" varchar(2) NOT NULL,
    "zip" varchar(50) NOT NULL,
    "since" date NOT NULL,
    "position" varchar(50),
    "lastip" inet NOT NULL,
    "emailme" boolean NOT NULL
)
;
CREATE TABLE "taxmodels_rate" (
    "id" serial NOT NULL PRIMARY KEY,
    "taxmodels_id" integer NOT NULL,
    "taxes_id" integer NOT NULL,
    UNIQUE ("taxmodels_id", "taxes_id")
)
;
CREATE TABLE "taxmodels" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(765) NOT NULL
)
;
ALTER TABLE "taxmodels_rate" ADD CONSTRAINT "taxmodels_id_refs_id_94a8fddf" FOREIGN KEY ("taxmodels_id") REFERENCES "taxmodels" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE TABLE "taxes" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(200) NOT NULL,
    "valid" boolean NOT NULL,
    "rate" numeric(5, 3) NOT NULL
)
;
ALTER TABLE "taxmodels_rate" ADD CONSTRAINT "taxes_id_refs_id_f7d6a5b5" FOREIGN KEY ("taxes_id") REFERENCES "taxes" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE TABLE "product_units" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(255) NOT NULL
)
;
CREATE TABLE "products" (
    "id" serial NOT NULL PRIMARY KEY,
    "barcode" varchar(100),
    "alphacode" varchar(90),
    "name" varchar(765) NOT NULL,
    "description" text,
    "providerid_id" integer NOT NULL REFERENCES "providers" ("id") DEFERRABLE INITIALLY DEFERRED,
    "category_id" integer NOT NULL REFERENCES "categories" ("id") DEFERRABLE INITIALLY DEFERRED,
    "taxmodel_id" integer NOT NULL REFERENCES "taxmodels" ("id") DEFERRABLE INITIALLY DEFERRED,
    "price" double precision NOT NULL,
    "units_id" integer NOT NULL REFERENCES "product_units" ("id") DEFERRABLE INITIALLY DEFERRED,
    "photo" text
)
;
CREATE TABLE "stock" (
    "id" serial NOT NULL PRIMARY KEY,
    "productid_id" integer NOT NULL REFERENCES "products" ("id") DEFERRABLE INITIALLY DEFERRED,
    "providerid_id" integer NOT NULL REFERENCES "providers" ("id") DEFERRABLE INITIALLY DEFERRED,
    "qty" double precision NOT NULL,
    "usingstock" boolean NOT NULL
)
;
CREATE TABLE "order_status" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(75) NOT NULL
)
;
CREATE TABLE "orders" (
    "id" serial NOT NULL PRIMARY KEY,
    "clientid_id" integer NOT NULL REFERENCES "clients" ("id") DEFERRABLE INITIALLY DEFERRED,
    "datetime" timestamp with time zone NOT NULL,
    "position" varchar(200),
    "subtotal" double precision NOT NULL,
    "totaltax" double precision NOT NULL,
    "eta" integer
)
;
CREATE TABLE "sub_orders" (
    "id" serial NOT NULL PRIMARY KEY,
    "orderid_id" integer NOT NULL REFERENCES "orders" ("id") DEFERRABLE INITIALLY DEFERRED,
    "providerid_id" integer NOT NULL REFERENCES "providers" ("id") DEFERRABLE INITIALLY DEFERRED,
    "label" varchar(500) NOT NULL,
    "status_id" integer NOT NULL REFERENCES "order_status" ("id") DEFERRABLE INITIALLY DEFERRED,
    "comments" text,
    "eta" integer
)
;
CREATE TABLE "order_items" (
    "id" serial NOT NULL PRIMARY KEY,
    "orderid_id" integer NOT NULL REFERENCES "orders" ("id") DEFERRABLE INITIALLY DEFERRED,
    "suborderid_id" integer NOT NULL REFERENCES "sub_orders" ("id") DEFERRABLE INITIALLY DEFERRED,
    "providerid_id" integer NOT NULL REFERENCES "providers" ("id") DEFERRABLE INITIALLY DEFERRED,
    "productid_id" integer NOT NULL REFERENCES "products" ("id") DEFERRABLE INITIALLY DEFERRED,
    "name" varchar(250) NOT NULL,
    "qty" double precision NOT NULL,
    "price" double precision NOT NULL,
    "tax" double precision NOT NULL
)
;
CREATE TABLE "invoice_types" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(30) NOT NULL
)
;
CREATE TABLE "invoices" (
    "id" serial NOT NULL PRIMARY KEY,
    "orderid_id" integer NOT NULL REFERENCES "orders" ("id") DEFERRABLE INITIALLY DEFERRED,
    "total" double precision NOT NULL,
    "subtotal" double precision NOT NULL,
    "totaltax" double precision NOT NULL,
    "tendered" double precision NOT NULL,
    "change" double precision NOT NULL,
    "typeid_id" integer NOT NULL REFERENCES "invoice_types" ("id") DEFERRABLE INITIALLY DEFERRED,
    "cardTerm" varchar(4),
    "disc" double precision NOT NULL,
    "discm" double precision NOT NULL
)
;
CREATE INDEX "cities_state_id" ON "cities" ("state_id");
CREATE INDEX "providers_category_id" ON "providers" ("category_id");
CREATE INDEX "clients_city_id" ON "clients" ("city_id");
CREATE INDEX "products_providerid_id" ON "products" ("providerid_id");
CREATE INDEX "products_category_id" ON "products" ("category_id");
CREATE INDEX "products_taxmodel_id" ON "products" ("taxmodel_id");
CREATE INDEX "products_units_id" ON "products" ("units_id");
CREATE INDEX "stock_productid_id" ON "stock" ("productid_id");
CREATE INDEX "stock_providerid_id" ON "stock" ("providerid_id");
CREATE INDEX "orders_clientid_id" ON "orders" ("clientid_id");
CREATE INDEX "sub_orders_orderid_id" ON "sub_orders" ("orderid_id");
CREATE INDEX "sub_orders_providerid_id" ON "sub_orders" ("providerid_id");
CREATE INDEX "sub_orders_status_id" ON "sub_orders" ("status_id");
CREATE INDEX "order_items_orderid_id" ON "order_items" ("orderid_id");
CREATE INDEX "order_items_suborderid_id" ON "order_items" ("suborderid_id");
CREATE INDEX "order_items_providerid_id" ON "order_items" ("providerid_id");
CREATE INDEX "order_items_productid_id" ON "order_items" ("productid_id");
CREATE INDEX "invoices_orderid_id" ON "invoices" ("orderid_id");
CREATE INDEX "invoices_typeid_id" ON "invoices" ("typeid_id");
COMMIT;
