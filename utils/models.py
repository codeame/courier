# -*- coding: utf-8 -*-
# MIGUEL CHAVEZ GAMBOA 2010.
# Courier system data models

#The database is not relational. Later will test relations on tables.

from django.db import models
from datetime import datetime
from django.contrib.localflavor.us.models import USStateField
from django.contrib.localflavor.us.models import PhoneNumberField
#from django.contrib.localflavor.us.forms import USStateField

class Categories(models.Model):
    name = models.CharField(max_length=765)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'categories'
        verbose_name_plural = "categories"

class States(models.Model):
    name = models.CharField(max_length=255)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'states'
        verbose_name_plural = "states"

class Cities(models.Model):
    name = models.CharField(max_length=255)
    state= models.ForeignKey(States)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'cities'
        verbose_name_plural = "cities"

class ProviderCategories(models.Model):
    name = models.CharField(max_length=250) #MexicanFood, ChineseFood ...
    def __unicode__(self):
        return self.name
    class Meta:
        db_table= 'provider_categories'
        verbose_name_plural = "provider categories"

class Providers(models.Model):
    name = models.CharField(max_length=765)
    email= models.EmailField()
    phone= PhoneNumberField() #models.CharField(max_length=50, blank=True, null=True)
    # Providers can have many locations in a city, to choose from for the supply.
    #addr = models.CharField(max_length=765) 
    #city = models.IntegerField() #CharField(max_length=765) 
    #state= models.IntegerField() # models.CharField(max_length=765)
    #zip  = models.CharField(max_length=765) # defined as charfield... on some countries its not numeric
    #pos  = models.CharField(max_length=50)  # longitude,latitude
    category = models.ForeignKey(ProviderCategories)
    # Do providers/restaurants are allowed (needed) to login to a site and add products or do something else?
    #password = models.CharField(max_length=765)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'providers'
        verbose_name_plural = "providers"


class Clients(models.Model):
    firstname = models.CharField(max_length=255)
    lastname  = models.CharField(max_length=255)
    # ADD A COMPANY NAME??? For corporative clients or clients delivering at their office
    #company   = models.CharField(max_length=255)
    email     = models.EmailField()  #email can be used for login
    password  = models.CharField(max_length=765)
    phone     = PhoneNumberField() #models.CharField(max_length=50)
    addr1     = models.CharField(max_length=765)
    addr2     = models.CharField(max_length=765)
    city      = models.ForeignKey(Cities) #models.CharField(max_length=50)
    state     = USStateField() #models.CharField(max_length=50) #Use USStateField or the states table?
    zip       = models.CharField(max_length=50) # The zip is related to the city?
    # ADD A DELIVERY ADDRESS ?
    since     = models.DateField(default=datetime.now)
    position  = models.CharField(max_length=50, blank=True, null=True) # latitude,longitude
    lastip    = models.IPAddressField(default="192.168.0.1") # to store the last ip addres the client logged in : Useful for online stores
    #signup for a news letter and special offers campaigns ?
    emailme   = models.BooleanField(default = False)
    def __unicode__(self):
        return self.lastname+', '+self.firstname #Asi o al reves lastname, firstname
    class Meta:
        db_table = 'clients'
        verbose_name_plural = "clients"

class TaxModels(models.Model):  # this is a container (group) for taxes.
    name = models.CharField(max_length=765)
    rate = models.ManyToManyField('Taxes', null=True)
    #models.CharField(max_length=200, blank=True, null=True) #contains a CSV for taxes ids (below) Why not use CommaSeparatedIntegerField? portability
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'taxmodels'
        verbose_name_plural = "tax models"
    
class Taxes(models.Model):
    name = models.CharField(max_length=200)
    valid= models.BooleanField(default=True)
    rate = models.DecimalField(max_digits=5, decimal_places=3) # DecimalField vs FloatField ... which to use?  Decimal uses NUMERIC data type :)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'taxes'
        verbose_name_plural = "taxes"

class ProductUnits(models.Model):
    name  = models.CharField(max_length=255)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'product_units'
        verbose_name_plural = "product units"
    
class Products(models.Model):
    barcode = models.CharField(max_length=100, blank=True, null=True) # a barcode
    alphacode = models.CharField(max_length=90, blank=True, null=True)
    name = models.CharField(max_length=765)
    description = models.TextField(blank=True, null=True)
    providerid = models.ForeignKey(Providers)
    category = models.ForeignKey(Categories)
    taxmodel = models.ForeignKey(TaxModels)
    price = models.FloatField()
    #cost = models.FloatField()
    units = models.ForeignKey(ProductUnits)
    photo = models.TextField(blank=True, null=True) # USE ImageField ??? PIL
    #typeid = models.SmallIntegerField() # Normal=0/Group=1/Raw=2
    #groupelements = models.CharField(max_length=3000, blank=True)
    #grouppricedrop = models.FloatField()
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'products'
        verbose_name_plural = "stock"


class Stock(models.Model):
    productid  = models.ForeignKey(Products)
    providerid = models.ForeignKey(Providers)
    #locationid
    qty        = models.FloatField()
    usingstock = models.BooleanField(default=False)
    class Meta:
        db_table = 'stock'
        verbose_name_plural = "stock"


class OrderStatus(models.Model):
    name = models.CharField(max_length=75)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'order_status'
        verbose_name_plural = "order status"

class Orders(models.Model):
    clientid   = models.ForeignKey(Clients)
    datetime   = models.DateTimeField(default=datetime.now)
    position   = models.CharField(max_length=200, blank=True, null=True, help_text="Use: <em>longitude,latitude</em>") #longitude, latitude
    subtotal   = models.FloatField()
    totaltax   = models.FloatField()
    eta        = models.IntegerField(null=True,blank=True,help_text="Estimated time for arrival, leave blank, software will set this.") # Estimated time of Arrival, in minutes.
    
    def _get_client_name(self):
        "Returns the client's full name."
        client = Clients.objects.get(pk=self.clientid_id)
        return unicode('%s, %s' % (client.lastname,client.firstname))
    client_name = property(_get_client_name)

    def _get_suborders(self):
        "Returns the suborders data"
        suborders = SubOrders.objects.filter(orderid=self.pk)
        return suborders
    suborders = property(_get_suborders)
    
    def __unicode__(self):
        "Returns the unicode description for the order"
        client = Clients.objects.get(pk=self.clientid_id)
        return unicode('Order No. %s - %s, %s' % (str(self.pk),client.lastname,client.firstname))

    class Meta:
        db_table = 'orders'
        verbose_name_plural = "orders"

class SubOrders(models.Model):
    orderid    = models.ForeignKey(Orders)
    providerid = models.ForeignKey(Providers)
    label      = models.CharField(max_length=500)
    status     = models.ForeignKey(OrderStatus) # Authorized (paid), Unconfirmed,Confirmed,InProgress,ReadyToGo,OnTheWay,Delivered.
    comments   = models.TextField(blank=True, null=True)
    eta        = models.IntegerField(null=True,blank=True,help_text="Used for restaurant time to prepare the order") # Estimated time of Arrival, in minutes.

    def _get_items(self):
        "Returns the Items in the suborder."
        myitems = OrderItems.objects.filter(suborderid=self.pk)
        return myitems
    items = property(_get_items)

    def __unicode__(self):
        return unicode('%s.%s / %s' % (str(self.orderid_id),str(self.pk),self.label))

    class Meta:
        db_table = 'sub_orders'
        verbose_name_plural = "sub orders"


#This is just for the "ticket" or "shipping content details" : TODO: name will cause duplicated data and not-updated also. REMOVE IT after BETA.
class OrderItems(models.Model):
    orderid    = models.ForeignKey(Orders) 
    suborderid = models.ForeignKey(SubOrders) 
    providerid = models.ForeignKey(Providers)
    productid  = models.ForeignKey(Products)
    name       = models.CharField(max_length=250)
    qty        = models.FloatField()
    price      = models.FloatField()
    tax        = models.FloatField()
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'order_items'
        verbose_name_plural = "order items"

class InvoiceTypes(models.Model):
    name = models.CharField(max_length=30) #models.CharField(choices=( ("Cash", "Cash"), ("Visa", "Visa"), ("MC", "Master Card"), ("Amex", "American Express") ), max_length=255)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'invoice_types'
        verbose_name_plural = "invoice types"

class Invoices(models.Model): # or Payments
    orderid = models.ForeignKey(Orders)
    total   = models.FloatField()
    subtotal= models.FloatField()
    totaltax= models.FloatField()
    tendered= models.FloatField() # on an online ordering system the payment is exact (credit card...)
    change  = models.FloatField() # and therefore there is no change to give, isnt it?.
    typeid  = models.ForeignKey(InvoiceTypes) # VISA MASTERCARD AMEX CASH  models.ForeignKey(InvoiceTypes)
    cardTerm= models.CharField(max_length=4, blank=True, null=True)
    disc    = models.FloatField()
    discm   = models.FloatField()
    class Meta:
        db_table = 'invoices'
        verbose_name_plural = "invoices"
