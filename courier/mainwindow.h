/***************************************************************************
 *   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
 *   miguel@lemonpos.org                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlRelationalTableModel>
#include <QSqlTableModel>
#include <QDateTime>

#include <marble/MarbleWidget.h>
#include <marble/MarbleControlBox.h>
#include <marble/RoutingWidget.h>

#include "../structs.h"

#define QT_USE_FAST_CONCATENATION
#define QT_USE_FAST_OPERATOR_PLUS

class MibitFloatPanel;
class MibitNotifier;


namespace Ui {
    class MainWindow;
}

using namespace Marble;



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    ///Shows configuration panel
    void showDBConfig();
    void showPrintConfig();
    ///Saves/Reads configuration information.
    void writeSettings();
    void readSettings();
    void saveSplitter();
    //quit
    void quit();
    //React when settings changed
    void settingsDBChanged();
    void setupDB();
    void connectToDb();
    void refreshModels();
    void enableConfirm();
    void numberClicked(int);
    void resetETA();
    void manualClicked();
    void showOrderDetails(const QModelIndex &index);
    void pickUpOrder();
    void setOrderAsArrived();
    void setOrderAsDelivered();
    void showPageDetails();
    void showMainPage();
    void showConfirmPanel();
    void updatePickInfo();
    void updateOrderDetails();
    void setOrderAsUndelivered();
    void checkUndeliverReason();
    void updateCourierName();
    void displayError(const QString &msg);
    void authenticate();
    void showInterruptPanel();
    void interruptDelivery();
    void checkReason(const QModelIndex &index);
    void callHome();

    void setupMarble();
    void changeSlider(int);
    void displayPosition(QString);
    void requestRoute(QList<GeoDataCoordinates> positions);
    void routeWidgetToggle(bool);
    void routeReady(GeoDataDocument* gdDocument);
    void getWholeRoute();
    void blockUI();
    void resumeOperations();
    void checkForInterruptions();

    void rmStateChanged(RoutingManager::State newState, RouteRequest *req);
    void setPlaces();

private:
    Ui::MainWindow *ui;
    MibitFloatPanel *groupPanel, *readyPanel, *printPanel, *undeliveredPanel, *confirmPanel, *interruptPanel;
    MibitNotifier  *errorMsg;
    QSqlDatabase db;
    QSqlTableModel *ordersModel;
    QSqlRelationalTableModel *subordersModel;
    QSqlTableModel * interruptionsModel;
    bool   modelsCreated;
    qulonglong orderId;
    //int etaValue;
    //int etaMin, etaHrs;
    QTimer *timerPick;
    QTimer *timerOrder;
    QTimer *pingTimer;
    MarbleWidget *theMap;
    RoutingWidget    *theRoutingWidget;
    QDateTime  startTime;
    int etaValue;
    int etaMin, etaHrs;

    //QList<GeoDataCoordinates> viaPoints;
    QList<LCDestinationInfo> viaPoints;
    GeoDataCoordinates clientPosition;

    QString courierId;
    QString courierKey;
    bool    courierAuthenticated;
    bool    deliveryInProgress;

    void setupModel();

protected:
     void closeEvent(QCloseEvent *event);

 signals:
    void allRoutesRetrieved();
    void nextPointSelected();
    void orderingFinished();
    void digitClicked(int);

};

#endif // MAINWINDOW_H
