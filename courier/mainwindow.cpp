/***************************************************************************
 *   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
 *   miguel@lemonpos.org                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#include <QApplication>
#include <QLocale>
#include <QDebug>
#include <QTimer>
#include <QTextCodec>
#include <QtSql>
#include <QSettings>
#include <QIntValidator>
#include <QSplitter>


#include <marble/global.h>
#include <marble/AbstractFloatItem.h>
#include <marble/MarbleModel.h>
#include <marble/RoutingManager.h>
//#include <marble/RoutingProfilesModel.h>
#include <marble/RouteRequest.h>
#include <marble/RoutingModel.h>
#include <marble/RoutingLayer.h>
#include <marble/GeoDataDocument.h>

#include <marble/AlternativeRoutesModel.h>


//#include <valgrind/callgrind.h>

#include "../printing/print-dev.h"
#include "../structs.h"
#include "orderdelegate.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../azahar.h"
#include "../mibitWidgets/mibitfloatpanel.h"
#include "../mibitWidgets/mibitnotifier.h"
#include "../misc.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //fullscreen
    //setWindowState( windowState() | Qt::WindowFullScreen ); // set
    //fullscreen OFF
    //setWindowState( windowState() & ~Qt::WindowFullScreen ); // reset

    qDebug()<<tr("Starting courier...");
    readSettings();
    modelsCreated=false;
    courierAuthenticated = false;
    deliveryInProgress = false;
    courierId = "";
    courierKey= "";
    etaValue = 0;
    etaMin = etaHrs = 0;

    //getting images path.
    QString imgPath = Misc::getAppDataPath()+"images/";
    QString icoPath = Misc::getAppIconPath();

    //set Icon.
    setWindowIcon( QIcon(icoPath+"courierApp.png") );

    //Assing controls to panels.
    groupPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top);
    groupPanel->setSize(311,250);
    groupPanel->addWidget(ui->groupConfig);
    groupPanel->setMode(pmManual);
    groupPanel->setHiddenTotally(true);
    groupPanel->hide();

    readyPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top );
    readyPanel->setSize(250,150);
    readyPanel->addWidget(ui->frameDetails); //ui->groupReady
    readyPanel->setMode(pmManual);
    readyPanel->setHiddenTotally(true);
    readyPanel->hide();

    printPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top );
    printPanel->setSize(250,250);
    printPanel->addWidget(ui->printConfig);
    printPanel->setMode(pmManual);
    printPanel->setHiddenTotally(true);
    printPanel->hide();

    undeliveredPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top );
    undeliveredPanel->setSize(320,270);
    undeliveredPanel->addWidget(ui->panelUndelivered);
    undeliveredPanel->setMode(pmManual);
    undeliveredPanel->setHiddenTotally(true);
    undeliveredPanel->hide();

    confirmPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top );
    confirmPanel->setSize(600,550);
    confirmPanel->addWidget(ui->groupConfirm);
    confirmPanel->setMode(pmManual);
    confirmPanel->setHiddenTotally(true);
    confirmPanel->hide();

    interruptPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top );
    interruptPanel->setSize(300,250);
    interruptPanel->addWidget(ui->groupInterrupt);
    interruptPanel->setMode(pmManual);
    interruptPanel->setHiddenTotally(true);
    interruptPanel->hide();

    QPixmap iconError = QPixmap(imgPath+"dialog-error.png");
    errorMsg = new MibitNotifier(this, imgPath+"rotated_panel_top.svg", iconError , false);
    errorMsg->setSize(500,150); //this->width()-100
    errorMsg->hide();

    //creating the marble widget and friends.

    //set the smallscreen profile.
    MarbleGlobal::Profiles profiles = MarbleGlobal::detectProfiles();
    profiles |= MarbleGlobal::SmallScreen;
    MarbleGlobal::getInstance()->setProfiles( profiles );

    QHBoxLayout *layout = new QHBoxLayout;
    QSplitter *mapSplitter = new QSplitter( this );
    layout->addWidget( mapSplitter );
    theMap = new MarbleWidget(ui->mapOnly);
    theRoutingWidget = new RoutingWidget(theMap,this);
    layout->addWidget(theRoutingWidget);
    theMap->setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding ) );
    layout->addWidget(theMap);
    ui->mapOnly->setLayout(layout);
    theMap->setAnimationsEnabled(false);
    theMap->setDefaultAngleUnit(Marble::DecimalDegree); //this shows only "2.21° W, 19.1° N" and not the minutes,seconds...
    ui->zoomSlider->setMaximum(3200);
    ui->zoomSlider->setMinimum(1000);
    ui->zoomSlider->setValue(1000);
    theMap->zoomView(1000);
    mapSplitter->addWidget( theRoutingWidget );
    mapSplitter->setStretchFactor( mapSplitter->indexOf( theRoutingWidget ), 0 );
    mapSplitter->addWidget( theMap );
    mapSplitter->setStretchFactor( mapSplitter->indexOf( theMap ), 1 );
    mapSplitter->setSizes( QList<int>() << 180 << width() - 180 );

    //ui->zoomSlider->hide();
    theRoutingWidget->hide();
    //loading logos.
    ui->lblLemon->setPixmap(QPixmap(imgPath+"lemonlogo.png"));

    connect( ui->btnSaveConfig, SIGNAL(clicked()), groupPanel, SLOT(hidePanel()) );
    connect( ui->btnSaveConfig, SIGNAL(clicked()), this, SLOT(writeSettings()) );
    connect( ui->btnSaveConfig, SIGNAL(clicked()), this, SLOT(settingsDBChanged()));
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), this, SLOT(writeSettings()));
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), printPanel, SLOT(hidePanel()) );
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), this, SLOT( updateCourierName() ) );
    connect( ui->actionShow_Settings, SIGNAL(triggered()), this, SLOT(showDBConfig()));
    connect( ui->actionShow_Printer_Settings, SIGNAL(triggered()), this, SLOT(showPrintConfig()));
    connect( ui->actionExit, SIGNAL(triggered()), this, SLOT(quit()));
    connect(ui->mapSplitter, SIGNAL(splitterMoved(int, int)), this, SLOT(saveSplitter()));
    connect(ui->orderList, SIGNAL(clicked(const QModelIndex &)), SLOT(showOrderDetails(const QModelIndex &)) );
    connect(ui->btnInterrupt, SIGNAL(clicked()), this, SLOT( showInterruptPanel() ) );
    connect(ui->btnCancelInterruption, SIGNAL(clicked()), interruptPanel, SLOT( hidePanel() ) );
    connect(ui->btnInterruptNow, SIGNAL(clicked()), this, SLOT( interruptDelivery() ) );
    connect(ui->iReasonsList, SIGNAL(clicked(const QModelIndex &)), SLOT(checkReason(const QModelIndex &)));

    connect(ui->btnPickUp, SIGNAL(clicked()), this, SLOT( showConfirmPanel() ) ); //pickUpOrder()
    connect(ui->btnConfirmCancel, SIGNAL(clicked()), confirmPanel, SLOT( hidePanel()) );
    connect(ui->btnConfirmCancel, SIGNAL(clicked()), this, SLOT( resetETA()) );
    connect(ui->btnConfirm, SIGNAL(clicked()), this, SLOT( pickUpOrder() ));
    connect(ui->btnReset, SIGNAL(clicked()), this, SLOT( resetETA() ));
    connect(ui->btnManual, SIGNAL(toggled(bool)), this, SLOT( manualClicked() ));

    connect(ui->btnNo, SIGNAL(clicked()), readyPanel, SLOT(hidePanel()) );
    connect(ui->btnNo, SIGNAL(clicked()), this, SLOT(showMainPage()) );
    //connect(ui->btnNo, SIGNAL(clicked()), this, SLOT(clearRoute()) );
    connect(ui->btnArrived, SIGNAL(clicked()), this, SLOT(setOrderAsArrived()) );
    connect(ui->btnDelivered, SIGNAL(clicked()), this, SLOT(setOrderAsDelivered()) );
    //connect(ui->btnDelivered, SIGNAL(clicked()), this, SLOT(clearRoute()));
    connect(ui->btnUndelivered, SIGNAL(clicked()), undeliveredPanel, SLOT(showPanel()));
    connect(ui->btnUndelivered, SIGNAL(clicked()), readyPanel, SLOT(hidePanel()));
    connect(ui->btnUndeliver, SIGNAL(clicked()), undeliveredPanel, SLOT(hidePanel()) );
    connect(ui->btnUndeliver, SIGNAL(clicked()), this, SLOT( setOrderAsUndelivered() ) );
    connect(ui->btnUndelivered, SIGNAL(clicked()), this, SLOT( checkUndeliverReason() ) );
    connect(ui->rbIncomplete, SIGNAL(toggled(bool)), SLOT(checkUndeliverReason()) );
    connect(ui->rbInexistent, SIGNAL(toggled(bool)), SLOT(checkUndeliverReason()) );
    connect(ui->rbNotFound, SIGNAL(toggled(bool)), SLOT(checkUndeliverReason()) );
    connect(ui->rbRejected, SIGNAL(toggled(bool)), SLOT(checkUndeliverReason()) );
    connect(ui->rbLate, SIGNAL(toggled(bool)), SLOT(checkUndeliverReason()) );


    // MAP - ZOOM CONNECTIONS
    connect(ui->zoomSlider, SIGNAL(sliderMoved(int)), theMap, SLOT(zoomView(int)) );
    connect(theMap, SIGNAL(zoomChanged(int)), this, SLOT(changeSlider(int)) );
    // MAP  - COORDINATES CHANGES
    connect(theMap, SIGNAL(mouseMoveGeoPosition( QString )), this, SLOT(displayPosition( QString )));
    //THE ROUTE BUTTON
    connect(ui->btnShowRoutePanel, SIGNAL(toggled(bool)), SLOT(routeWidgetToggle(bool)) );

    //connecting keypad signals
    QSignalMapper *signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(digitClicked(int)));

    signalMapper->setMapping(ui->btn0, 0);
    connect(ui->btn0, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn1, 1);
    connect(ui->btn1, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn2, 2);
    connect(ui->btn2, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn3, 3);
    connect(ui->btn3, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn4, 4);
    connect(ui->btn4, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn5, 5);
    connect(ui->btn5, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn6, 6);
    connect(ui->btn6, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn7, 7);
    connect(ui->btn7, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn8, 8);
    connect(ui->btn8, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn9, 9);
    connect(ui->btn9, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn10, 10);
    connect(ui->btn10, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn15, 15);
    connect(ui->btn15, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn20, 20);
    connect(ui->btn20, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn25, 25);
    connect(ui->btn25, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn30, 30);
    connect(ui->btn30, SIGNAL(clicked()), signalMapper, SLOT(map()));

    connect(this, SIGNAL(digitClicked(int)), this, SLOT(numberClicked(int)));


    QTimer::singleShot(1000, this, SLOT(setupDB()));
    QTimer *updateTimer = new QTimer(this);
    updateTimer->setInterval(3000);
    connect(updateTimer, SIGNAL(timeout()), SLOT(refreshModels()) );
    updateTimer->start();
    pingTimer = new QTimer(this);
    pingTimer->setInterval(60000); //each minute.
    connect(pingTimer, SIGNAL(timeout()), SLOT( callHome() ));
    ui->rbHrs->hide();
    ui->rbMin->hide();
    ui->frames->setCurrentIndex(0);
    QTimer::singleShot(0,this,SLOT(setupMarble()));
    timerPick = new QTimer(this);
    timerPick->setInterval(1000);
    connect(timerPick, SIGNAL(timeout()), SLOT(updatePickInfo()) );
    timerOrder = new QTimer(this);
    timerOrder->setInterval(3000);
    connect(timerOrder, SIGNAL(timeout()), SLOT(updateOrderDetails()) );
    connect(ui->btnNo, SIGNAL(clicked()), timerOrder, SLOT(stop()) );
    statusBar()->showMessage(tr("Welcome to CourierApp. (C) 2010, Miguel Chavez Gamboa"));
    //qApp->setAutoSipEnabled(true); //Only has an effect on platforms which use software input panels, such as Windows CE and Symbian.

    ui->lblCourierName->setText( tr("Courier #%1").arg( ui->editCfgCourierId->text() ) );

    ui->frames->setDisabled(true); // disable while not authenticated.
    QTimer::singleShot(1000, this, SLOT(authenticate()));
    ui->actionResume->setDisabled(true);
    connect(ui->actionResume, SIGNAL(triggered()), SLOT( resumeOperations() )  );

}


void MainWindow::authenticate()
{
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    LCCourierInfo cInfo = myDb->getCourierInfo( ui->editCfgCourierId->text().toULongLong() );

    if (cInfo.key == ui->editCfgCourierKey->text()) {
        //Yes, they are the same.
        courierAuthenticated = true;
        ui->frames->setEnabled(true);
        courierId  = ui->editCfgCourierId->text();
        courierKey = ui->editCfgCourierKey->text();
        qDebug()<<"** Authenticating :: PASSED **";
        checkForInterruptions();
    } else {
        //NO, they are not the same key.
        courierAuthenticated = false;
        ui->frames->setDisabled(true);
        courierId  = "";
        courierKey = "";
        displayError(tr("Error, the courier key is invalid!"));
        qDebug()<<"** Authenticating :: FAILED **";
        //show the config panel
        showPrintConfig();
        ui->editCfgCourierKey->setFocus();
        ui->editCfgCourierKey->setSelection(0, ui->editCfgCourierKey->text().size()-1);
    }
}

void MainWindow::checkForInterruptions()
{
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);

    int count = myDb->interruptedOrders(ui->editCfgCourierId->text().toULongLong());

    if (count > 0) {
        //yes, the courier has more than 0 interrupted orders, so block the UI until the cause is resolved.
        blockUI();
    }

    delete myDb;
}

void MainWindow::blockUI()
{
    //block the Frame
    ui->frames->setDisabled(true);
    //enable Unblock action
    ui->actionResume->setEnabled(true);
    QString msg = tr("Operations Blocked because you have an interruption. When your problem is resolved, restart the application or resume operations from the menu.");
    //show an error message!
    statusBar()->showMessage(msg);
    displayError(msg);
}

void MainWindow::resumeOperations()
{
    ui->actionResume->setDisabled(true);
    ui->frames->setEnabled(true);
}

void MainWindow::showMainPage()
{
    ui->frames->setCurrentIndex(0);
}

void MainWindow::checkUndeliverReason()
{
    bool oneSelected = false;
    oneSelected = ( ui->rbIncomplete->isChecked()
                 || ui->rbInexistent->isChecked()
                 || ui->rbNotFound->isChecked()
                 || ui->rbRejected->isChecked()
                 || ui->rbLate->isChecked());

    ui->btnUndeliver->setEnabled(oneSelected);
}

void MainWindow::enableConfirm()
{
    if ( etaValue > 0 )
        ui->btnConfirm->setEnabled(true);
    else
        ui->btnConfirm->setEnabled(false);
}

void MainWindow::numberClicked(int n)
{
    //Check the edit mode.
    if (ui->btnAdd->isChecked())
        etaValue += n; // this approach sums the pressed numbers...
    else if (ui->btnSub->isChecked())
        etaValue -= n; // this approach substracts the pressed numbers...
    else {
        // set the number on the selected field, and calculate the etaValue
        if (ui->rbHrs->isChecked()) {
            //add hours
            etaValue = etaValue - (etaHrs*60) + n*60;
        }
        else { //if (ui->editEstimatedMinutes->hasFocus()) {
            //add minutes
            etaValue = etaValue - (etaMin) + n;
        }
    }

    etaMin = 0; etaHrs =0; //reset
    if (etaValue > 59 ) {
        etaHrs = etaValue / 60;
        etaMin = etaValue % 60;
    } else etaMin = etaValue;

    if (etaValue < 0 ) {
        etaValue = etaMin = etaHrs = 0;
        ui->btnAdd->setChecked(true); // set the ADD button enable, cos we are getting negative numbers.
    }

    qDebug()<<" ETA Value:"<<etaValue<<" ETA:"<<etaHrs<<":"<<etaMin;
    ui->editEstimatedHours->setText(QString::number(etaHrs));
    ui->editEstimatedMinutes->setText(QString::number(etaMin));
    enableConfirm();
}

void MainWindow::resetETA()
{
    etaValue = etaMin = etaHrs = 0;
}

void MainWindow::manualClicked()
{
    if (ui->btnManual->isChecked()) {
        ui->rbHrs->show();
        ui->rbMin->show();
    } else {
        ui->rbHrs->hide();
        ui->rbMin->hide();
    }
}

void MainWindow::displayError(const QString &msg)
{
    //launch the panel.
    errorMsg->setSize(this->width()-50,150);
    errorMsg->setTextColor("#ff5500");
    errorMsg->showNotification(msg);
}

void MainWindow::displayPosition(QString text)
{
    if (text == tr("not available")) {
        statusBar()->showMessage("CourierApp (C) 2010, Miguel Chavez Gamboa");
        return;
    }
    //Why displaying this sign � instead of ° ?  (caused by the QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8")) )
    QString newText = text.toLatin1();
    newText = tr("Position: ") + newText.replace("?", "°");
    statusBar()->showMessage(newText);
}

void MainWindow::changeSlider(int x)
{
    ui->zoomSlider->blockSignals(true);
    ui->zoomSlider->setValue(x);
    ui->zoomSlider->blockSignals(false);
}

void MainWindow::routeWidgetToggle(bool show)
{
    if (show) {
        theRoutingWidget->show();
        ui->btnShowRoutePanel->setText(tr("Hide Route Panel"));
        saveSplitter();
        ui->mapSplitter->setSizes(QList<int>() <<0 << ui->pageDetails->width());
    }
    else {
        theRoutingWidget->hide();
        ui->btnShowRoutePanel->setText(tr("Show Route Panel"));
        QSettings settings("lemonCourier_courier", "lemonPOS Courier");
        ui->mapSplitter->restoreState(settings.value("splitter").toByteArray());
    }
}

void MainWindow::updateCourierName()
{
    if (db.isOpen()) {
        Azahar *myDb = new Azahar();
        myDb->setDatabase(db);
        LCCourierInfo cInfo;
        //get courier name
        cInfo = myDb->getCourierInfo( ui->editCfgCourierId->text().toULongLong() );
        if (cInfo.key == ui->editCfgCourierKey->text())
            ui->lblCourierName->setText( tr("Courier: %1").arg(cInfo.name) );
        else
            ui->lblCourierName->setText( tr("Courier: Not Authenticated") );
    } else {
        ui->lblCourierName->setText( tr("Courier #%1").arg( ui->editCfgCourierId->text() ) );
    }
}

void MainWindow::setupMarble()
{
    qDebug()<<"Setting up Marble...";

    theMap->setMapThemeId("earth/openstreetmap/openstreetmap.dgml");
    theMap->setShowOverviewMap(false);
    theMap->setShowScaleBar(false);
    theMap->setShowCompass(true);
    theMap->setShowGrid(false);
    theMap->setShowAtmosphere(false);
    theMap->setShowGps(true);
    theMap->floatItem("routing")->setVisible( true );
    theMap->model()->routingManager()->setWorkOffline( true );

    theMap->setShowOtherPlaces(true);
    theMap->setShowPlaces(true);

    //connect the routerManager's routeRetrieved signal
    connect( theMap->model()->routingManager(), SIGNAL(routeRetrieved(GeoDataDocument*)), SLOT(routeReady(GeoDataDocument*)) );

    connect( theMap->model()->routingManager(), SIGNAL(stateChanged(RoutingManager::State, RouteRequest *)), SLOT(rmStateChanged(RoutingManager::State, RouteRequest *)) );


    //Just testing plugins
    QList<RenderPlugin *> pluginList = theMap->renderPlugins();
    QList<RenderPlugin *>::const_iterator it = pluginList.constBegin();
    QList<RenderPlugin *>::const_iterator const listEnd = pluginList.constEnd();
    for (; it != listEnd; ++it ) {
        qDebug()<<" iterando plugins: "<< (*it)->nameId();
    }

    //Set Routing Profile
    RoutingManager *routingManager = theMap->model()->routingManager();
    if (routingManager->profilesModel()->profiles().count() <= 0) {
        //NO PROFILES, add my profile. As soon as the new profile is added the profie is saved for later use.
        routingManager->profilesModel()->addProfile("LemonCourier Profile");
        //set the properties, first for routino.
        QHash<QString, QVariant> settings = routingManager->profilesModel()->profiles().first().pluginSettings()["routino"];
        if ( !settings.contains( "transport" ) ) {
            settings.insert( "transport", "motorcar" );
        }
        if ( !settings.contains( "method" ) ) {
            settings.insert( "method", "fastest" );
        }
        QHash< QString, QHash<QString, QVariant> > theSettings;
        theSettings.insert("routino", settings);
        //set the properties, now for yours. really?  not now!
        routingManager->profilesModel()->setProfilePluginSettings(0 ,theSettings);
    }

//    qDebug()<<"Routing Profiles #"<<routingManager->profilesModel()->profiles().count();
//    for (int i=0; i<routingManager->profilesModel()->profiles().count(); i++) {
//        qDebug()<<"Profile: "<<routingManager->profilesModel()->profiles().at(i).name();
//        //qDebug()<<"Profile: "<<routingManager->profilesModel()->profiles().at(i).
//    }
}

void MainWindow::rmStateChanged(RoutingManager::State newState, RouteRequest *req)
{
    qDebug()<<"routingModel::STATECHANGED() newState:"<<newState<<" req:"<<req->source().toString()<<" -> "<<req->destination().toString();
}


void MainWindow::showDBConfig()
{
    if ( deliveryInProgress ) return; //do not allow to configure when delivery is in progress.

    //load config
    readSettings();
    //show it...
    groupPanel->showPanel();
    //set focus to the db field
    ui->editCfgDatabase->setFocus();
}

void MainWindow::showPrintConfig()
{
    //load config
    readSettings();
    //show it...
    printPanel->showPanel();
    ui->editCfgCourierId->setFocus();
    //Do not allow change the CuirierId if delivery is in progress!
    if ( deliveryInProgress ) {
        ui->editCfgCourierId->setDisabled(true);
        ui->editCfgCourierKey->setDisabled(true);
    }
}

void MainWindow::showConfirmPanel()
{
    //clear edits
    ui->editEstimatedHours->clear();
    ui->editEstimatedMinutes->clear();

    subordersModel->setFilter(QString("%1.%2=%3").arg(SUBORDERS_TABLE).arg(SOC_ORDERID).arg(orderId));
    subordersModel->select();

    //show the dialog
    confirmPanel->showPanel();
    //disable confirm button
    ui->btnConfirm->setDisabled(true);
}

void MainWindow::showInterruptPanel()
{
    ui->btnInterruptNow->setDisabled(true);
    interruptPanel->showPanel();
    interruptionsModel->select();
}

void MainWindow::saveSplitter()
{
    QSettings settings("lemonCourier_courier", "lemonPOS Courier");
    settings.setValue("splitter", ui->mapSplitter->saveState());
}

void MainWindow::readSettings()
 {
     QSettings settings("lemonCourier_courier", "lemonPOS Courier");
     ui->editCfgDatabase->setText(settings.value("dbName", QString("lemondb")).toString());
     ui->editCfgHost->setText(settings.value("dbHost", QString("localhost")).toString());
     ui->editCfgPassword->setText(settings.value("dbPassword", QString("")).toString());
     ui->editCfgProvider->setCurrentIndex( ui->editCfgProvider->findText( (settings.value("dbProvider", QString("PostgreSQL")).toString()) , Qt::MatchExactly ) );
     ui->editCfgUser->setText(settings.value("dbUser", QString("lemonuser")).toString());

     ui->editCfgDevice->setText(settings.value("printerDevice", QString("/dev/lp0")).toString());
     ui->rbUseCUPS->setChecked( settings.value("printerUseCUPS", true).toBool() );
     ui->rbUseDEV->setChecked( settings.value("printerUseDEV", false).toBool() );

     ui->editCfgCourierId->setText(settings.value("courierID", QString("PLEASE ENTER YOUR ID")).toString());
     ui->editCfgCourierKey->setText(settings.value("courierKEY", QString("PLEASE ENTER YOUR KEY")).toString());

     restoreGeometry(settings.value("geometry").toByteArray());
     restoreState(settings.value("windowState").toByteArray());
     ui->mapSplitter->restoreState(settings.value("splitter").toByteArray());

 }

void MainWindow::writeSettings()
 {
     QSettings settings("lemonCourier_courier", "lemonPOS Courier");
     settings.setValue("dbName", ui->editCfgDatabase->text());
     settings.setValue("dbHost", ui->editCfgHost->text());
     settings.setValue("dbPassword", ui->editCfgPassword->text());
     settings.setValue("dbProvider", ui->editCfgProvider->currentText());
     settings.setValue("dbUser", ui->editCfgUser->text());

     settings.setValue("printerDevice", ui->editCfgDevice->text());
     settings.setValue("printerUseCUPS", ui->rbUseCUPS->isChecked() );
     settings.setValue("printerUseDEV", ui->rbUseDEV->isChecked() );

     settings.setValue("courierID", ui->editCfgCourierId->text().toULongLong());
     settings.setValue("courierKey", ui->editCfgCourierKey->text());

     if ( courierId != ui->editCfgCourierId->text() || courierKey != ui->editCfgCourierKey->text() || !courierAuthenticated ) {
         // authenticate again, because courierId or courierKey has changed or not authenticated yet.
         qDebug()<<"WriteSettings :: Authenticating...";
         authenticate();
     }

 }

/* NOTE: Config needs a keyboard!!!.. or use a virtual one.
   Once is configured, its not needed, so its no need to make available the config dialog  */
void MainWindow::settingsDBChanged()
{
  if (db.isOpen()) db.close();

  QString toCompare;
  qDebug()<<" Database STR ON COMBO:"<<ui->editCfgProvider->currentText()<<" Database still in use:"<<db.driverName();
  if (ui->editCfgProvider->currentText() == "MySQL") toCompare = "QMYSQL"; else toCompare = "QPSQL";

  if (toCompare != db.driverName() ) {
      qDebug()<<"Database driver was changed...  new driver:"<<db.driverName();
      if (modelsCreated) {
        delete subordersModel;
        delete ordersModel;
        delete interruptionsModel;
      }
      db.close();
      db.removeDatabase(db.connectionName());

      QString dbName = "";
      if (ui->editCfgProvider->currentText() == "MySQL")
          dbName = "QMYSQL";
      else //for now only mysql and psql
          dbName = "QPSQL";

      qDebug()<<"Adding database...";
      db = QSqlDatabase::addDatabase(dbName);
      qDebug()<<"Database Added: "<<db;

      // QSqlTableModels need to be created after adding the database ( db = addDatabase... )
      subordersModel = new QSqlRelationalTableModel();
      ordersModel     = new QSqlTableModel();
      interruptionsModel = new QSqlTableModel();
      modelsCreated   = true;

      db.setHostName(ui->editCfgHost->text());
      db.setDatabaseName(ui->editCfgDatabase->text());
      db.setUserName(ui->editCfgUser->text());
      db.setPassword(ui->editCfgPassword->text());
      connectToDb();
  } else {
      return;
  }

  //setupModel();
}

void MainWindow::setupDB()
{
  qDebug()<<"Setting up database...";
  if (db.isOpen()) db.close();
  //FIXME: The next line affects the Marble POSITION DEGREE SYMBOL!!!!!
  //       Its supposed to deal with strings saved in database as UTF-8
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

  QString dbName = "";
  if (ui->editCfgProvider->currentText() == "MySQL")
      dbName = "QMYSQL";
  else //for now only mysql and psql
      dbName = "QPSQL";

  qDebug()<<"DB DRIVERS:"<<db.drivers();
  qDebug()<<"Adding the new Database...";
  db = QSqlDatabase::addDatabase(dbName);
  db.setHostName(ui->editCfgHost->text());
  db.setDatabaseName(ui->editCfgDatabase->text());
  db.setUserName(ui->editCfgUser->text());
  db.setPassword(ui->editCfgPassword->text());

  connectToDb();
}

void MainWindow::connectToDb()
{
  if (!db.open()) {
    db.open(); //try to open connection
    qDebug()<<"(1/connectToDb) Trying to open connection to database..";
  }
  if (!db.isOpen()) {
    db.open(); //try to open connection again...
    qDebug()<<"(2/connectToDb) Trying to open connection to database..";
  }
  if (!db.isOpen()) {
    qDebug()<<"(3/connectToDb) Configuring..";
    groupPanel->showPanel();
  } else {
    qDebug()<<" CONNECTED. ";
    if (!modelsCreated) { //Create models...
      qDebug()<<"Creating model...";
      subordersModel = new QSqlRelationalTableModel();
      ordersModel     = new QSqlTableModel();
      interruptionsModel = new QSqlTableModel();
      modelsCreated   = true;
    }
    qDebug()<<"Calling setupModel()";
    setupModel();
  }
}


void MainWindow::setupModel()
{
  qDebug()<<"SetupModel...";
  if (!db.isOpen()) {
    connectToDb();
  }
  else {
    qDebug()<<"setting up database model";
    subordersModel->setTable(SUBORDERS_TABLE);
    ordersModel->setTable(ORDERS_TABLE);
    interruptionsModel->setTable(INTERRUPTIONS_TABLE);

    if (ordersModel->tableName() != ORDERS_TABLE)
      ordersModel->setTable(ORDERS_TABLE);
    if (subordersModel->tableName() != SUBORDERS_TABLE)
      subordersModel->setTable(SUBORDERS_TABLE);
    if (interruptionsModel->tableName() != INTERRUPTIONS_TABLE)
      interruptionsModel->setTable(INTERRUPTIONS_TABLE);

    ordersModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ordersModel->setHeaderData(ordersModel->fieldIndex(OC_ID), Qt::Horizontal, tr("Order"));
    ordersModel->setHeaderData(ordersModel->fieldIndex(OC_CLIENTID), Qt::Horizontal, tr("Client"));

    subordersModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    subordersModel->setRelation(subordersModel->fieldIndex(SOC_PROVID), QSqlRelation(PROVIDERS_TABLE, PRO_ID, PRO_NAME) );

    interruptionsModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->iReasonsList->setModel(interruptionsModel);
    ui->iReasonsList->setModelColumn(interruptionsModel->fieldIndex(INT_NAME));

    ui->providersList->setModel(subordersModel);
    ui->providersList->setModelColumn(subordersModel->fieldIndex(SOC_PROVID));
    ui->providersList->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->orderList->setModel(ordersModel);
    ui->orderList->setModelColumn(ordersModel->fieldIndex("id"));
    ui->orderList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    OrderDelegate *oDelegate = new OrderDelegate(ui->orderList, db);
    ui->orderList->setItemDelegate(oDelegate);

    ordersModel->setSort( ordersModel->fieldIndex(OC_STATUSID),Qt::DescendingOrder );
    updateCourierName();
    refreshModels();
  }
}

void MainWindow::refreshModels()
{
    if ( modelsCreated && db.isOpen() ) {
        //should we show the ready orders only? or show also confirmed (and order with 1 confirmed and 1 ready, for example).

        //check for orphan orders for the current courierId
        Azahar *myDb = new Azahar();
        myDb->setDatabase(db);
        qulonglong courierId = ui->editCfgCourierId->text().toULongLong();

        /** @note: Orders with a ping of more than 24 hrs and STATUS_PICKCONFIRMED or STATUS_ONTHEWAY are considered
                   STATUS_ORPHAN_WARNING, which are not displayed on the new orders list because it does not make any sense
                   to display orders that are suspended too much time.
                   The STATUS_ORPHAN_WARNING orders will be shown on the courierAdmin application.
                   The courierAdmin application is for monitoring such warning events, and more administrative tasks.
        **/

        if ( !myDb->getOrphanOrders(courierId).isEmpty() ) {
            ordersModel->setFilter( QString("%1.%2>=%3 AND %1.%4=%5") /** @note: REVISE IF ADDED MORE STATUS > 15 **/
                                    .arg(ORDERS_TABLE).arg(OC_STATUSID)
                                    .arg(STATUS_ORPHAN_WARNING).arg(OC_COURIERID).arg(courierId));
        } else {
            ordersModel->setFilter( QString("%1.%2=%3 OR %1.%2=%4 OR (%1.%2>%5 AND %1.%2<=%6)")
                                .arg(ORDERS_TABLE).arg(OC_STATUSID)
                                .arg(STATUS_READY).arg(STATUS_CONFIRMED)
                                .arg(STATUS_INTERRUPTED).arg(REASON_INT_TRAFFICEME));
        }

        subordersModel->setFilter(QString("%1.%2=%3").arg(SUBORDERS_TABLE).arg(SOC_ORDERID).arg(orderId));

        subordersModel->select();
        ordersModel->select();
    }
}


void MainWindow::showOrderDetails(const QModelIndex &index)
{
    qulonglong row = index.row();
    QModelIndex indx = ordersModel->index(row, ordersModel->fieldIndex(OC_ID));
    orderId = ordersModel->data(indx, Qt::DisplayRole).toULongLong();
    ui->btnArrived->hide();
    ui->btnDelivered->hide();
    ui->btnUndelivered->hide();
    ui->btnInterrupt->hide();
    showPageDetails();
    timerOrder->start();
}

void MainWindow::pickUpOrder()
{
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    //in orderId we have the orderid to change status

    // order.eta is:
    //   time to arrive, from the courier position to the client (picking each suborder).
    //   this time will be updated at each stage.

    if (myDb->setOrderStatus( orderId, STATUS_PICKCONFIRMED, ui->editCfgCourierId->text().toULongLong(), false )) { //status will be a key of OrderStatus table, take care of this.
        myDb->setOrderETA( orderId, etaValue ); //update eta time to the order (not suborders).
        //then, start timer, update labels.
        ui->lblStartTime->setText( QTime::currentTime().toString("hh:mm ap") );
        ui->lblElapsedTime->setText("00:00:00");
        ui->lblSOPicked->setText("None");
        startTime = QDateTime::currentDateTime();
        timerPick->start();
        pingTimer->start();
        callHome();//first ping!
        confirmPanel->hidePanel();
        readyPanel->showPanel();
        //disallow cancel.
        ui->btnNo->setDisabled(true);
        ui->btnPickUp->setDisabled(true);
        ui->btnPickUp->hide();
        ui->btnArrived->show();
        ui->btnInterrupt->show();
        ui->actionResume->setDisabled(true);
        //model update
        ordersModel->select();
        deliveryInProgress = true;
    } else {
        //inform that the database connection may not being working..
        displayError(tr("Could not execute the database modification:\n%1").arg(myDb->lastError()));
        deliveryInProgress = false;
    }
    delete myDb;

    resetETA();
}

void MainWindow::setOrderAsArrived()
{
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    //now do the database modification (order arrived to client's address) but not delivered yet
    if (myDb->setOrderStatus( orderId, STATUS_ARRIVED, ui->editCfgCourierId->text().toULongLong() )) { //status will be a key of OrderStatus table, take care of this.
        myDb->setOrderETA(orderId, 0); //just arrived!
        ui->btnArrived->hide();
        ui->btnDelivered->show();
        ui->btnUndelivered->show();
        ui->btnInterrupt->hide();
        ui->btnNo->hide();
        timerPick->stop();
        ordersModel->select();
    } else {
        //inform that the database connection may not being working..
        displayError(tr("Could not execute the database modification:\n%1").arg(myDb->lastError()));
    }
    delete myDb;
}

void MainWindow::setOrderAsDelivered()
{
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    if (myDb->setOrderStatus( orderId, STATUS_DELIVERED, ui->editCfgCourierId->text().toULongLong() )) { //status will be a key of OrderStatus table, take care of this.
        ui->btnDelivered->hide();
        ui->btnUndelivered->hide();
        ui->btnNo->show();
        ui->btnPickUp->show();
        ordersModel->select();
        showMainPage();
        readyPanel->hidePanel();
        timerOrder->stop();
        pingTimer->stop();
        ui->btnPickUp->setEnabled(true);
        ui->btnNo->setEnabled(true);
        deliveryInProgress = false;
    } else {
        //inform that the database connection may not being working..
        displayError(tr("Could not execute the database modification:\n%1").arg(myDb->lastError()));
        deliveryInProgress = true;
    }
    delete myDb;
}

void MainWindow::setOrderAsUndelivered()
{
    //NOTE: Here is missing to save a reason for the underlivering.
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    if (myDb->setOrderStatus( orderId, STATUS_UNDELIVERED, ui->editCfgCourierId->text().toULongLong() )) { //status will be a key of OrderStatus table, take care of this.
        ui->btnDelivered->hide();
        ui->btnUndelivered->hide();
        ui->btnNo->show();
        ui->btnPickUp->show();
        showMainPage();
        readyPanel->hidePanel();
        timerOrder->stop();
        ui->btnPickUp->setEnabled(true);
        ui->btnNo->setEnabled(true);
        deliveryInProgress = false;
        pingTimer->stop();
    } else {
        //inform that the database connection may not being working..
        displayError(tr("Could not execute the database modification:\n%1").arg(myDb->lastError()));
        deliveryInProgress = true;
    }
    delete myDb;
}

void MainWindow::checkReason(const QModelIndex &index)
{
    ui->btnInterruptNow->setEnabled(index.isValid());
}

void MainWindow::callHome()
{
    qDebug()<<"calling home";
    if (true) { // place a config.usePing when available
        Azahar *myDb = new Azahar();
        myDb->setDatabase(db);

        if ( myDb->setPing(orderId) )
            qDebug()<<"Ping to home sucessful...";
        else
            qDebug()<<"Could not ping to home!, network problem?...";

        delete myDb;
    }
}

void MainWindow::interruptDelivery()
{
    //save interruption reason (get the reason, keyboard?)
    QModelIndex indx = ui->iReasonsList->currentIndex();
    QString reason   = indx.data( Qt::DisplayRole ).toString();
    int row          = indx.row();
    indx             = interruptionsModel->index(row, interruptionsModel->fieldIndex(INT_ID));
    qulonglong  rId  = interruptionsModel->data(indx, Qt::DisplayRole).toULongLong();
    int status       = STATUS_INTERRUPTED + rId;
    qDebug()<<"ROW:"<<row<<"Reason ID:"<<rId<<" Reason:"<<reason<<" ORDER ID:"<<orderId<<" Final Status:"<<status;

    if ( rId > 0) {
        Azahar *myDb = new Azahar();
        myDb->setDatabase(db);
        //this sets the order status, but not the suborder status.
        if ( myDb->setOrderStatus(orderId, status ) ) {
            //close the panel
            interruptPanel->hidePanel();
            deliveryInProgress = false;
            ui->btnNo->show();
            ui->btnNo->setEnabled(true);
            ui->btnPickUp->show();
            ui->btnInterrupt->hide();
            readyPanel->hidePanel();
            timerOrder->stop();
            timerPick->stop();
            pingTimer->stop();
            showMainPage();
            //Block the UI
            blockUI();
        } else {
            //show an error
            displayError(tr("Could not execute the database modification:\n%1").arg(myDb->lastError()));
            deliveryInProgress = true;
        }
        delete myDb;
    }
}

//Could this method be executed once per minute? Do not draw the seconds in the timer.
void MainWindow::updatePickInfo()
{
    QDateTime now = QDateTime::currentDateTime();
    int elapsedSeconds =  startTime.secsTo(now);
    QTime elapsed;
    elapsed = elapsed.addSecs(elapsedSeconds);
    ui->lblElapsedTime->setText(elapsed.toString("hh:mm:ss"));

    //is it good to query the database once a second (this method is executed each second).
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    int pickedCount = myDb->getOrderPickedItemsCount(orderId);
    int totalCount  = myDb->getOrderItemsCount(orderId);

    ui->lblSOPicked->setText( tr("%1 of %2").arg(pickedCount).arg(totalCount) );

    delete myDb;
}

void MainWindow::updateOrderDetails()
{
    //get data to show
    LCPrintOrderInfo order;
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    order = myDb->getPrintOrderInfo(orderId);

    /** @todo: Issue #13: Update order.eta.
               How to calculate time, based on route's distance remaining?..
               * GPS Velocity?
               * From the marble?  Marble::RoutingModel::duration(),  Marble::RoutingModel::remainingTime(), Marble::RoutingModel::totalTimeRemaining()
    **/
   //qDebug()<<" Duration:"<< theMap->model()->routingManager()->routingModel()->duration().time;
   //qDebug()<<" Duration:"<< theMap->model()->routingManager()->routingModel()->remainingTime();
   //qDebug()<<" Duration:"<< theMap->model()->routingManager()->routingModel()->totalTimeRemaining();

    /** @todo: complete this code with GPS data **/
    //myDb->setOrderETA(orderId, newETA );

    //writing the order details text
    QString text = QString("<b><span style=\" font-size:13pt;\">%1 %2 - %3 </span></b><br> &nbsp;&nbsp;&nbsp;&nbsp;<i>%4</i><br>&nbsp;&nbsp;&nbsp;&nbsp;%5 %6 <ul>")
                   .arg(tr("Order No.")).arg(order.orderid).arg(order.clientName)
                   .arg(order.clientAddr)
                   .arg(order.subordersList.count())
                   .arg(tr("Sub Orders From:"));
    //get each sub order details.
    foreach(LCSubOrderInfo soi, order.subordersList ) {
        if (soi.statusid == STATUS_ONTHEWAY || soi.statusid == STATUS_ARRIVED) {
            text += QString("<li><span style=\"text-decoration: line-through;\"><b>%1</b><span style=\" color:#ff5500;\"><i> Picked </i></span>.</span></li> <ul> ")
                    .arg( myDb->getProviderName(soi.providerid));
        }
        else if ( (soi.statusid == STATUS_READY || soi.statusid == STATUS_PICKCONFIRMED) && soi.eta <= 0) {
            text += QString("<li><b>%1</b>,<span style=\" color:#ff5500;\"><i> %2 </i></span>.</span></li> <ul> ")
                .arg( myDb->getProviderName(soi.providerid))
                .arg(tr("ready"));
        }
        else {
            text += QString("<li><b>%1</b>, %2 <span style=\" color:#ff5500;\"><i>%3 min</i></span>.</span></li> <ul> ")
                .arg( myDb->getProviderName(soi.providerid))
                .arg(tr("ready in"))
                .arg(soi.eta);
        }
        //each suborder element
        foreach(LCPrintOrderItems itm, soi.items ) {
            if (soi.statusid == STATUS_ONTHEWAY || soi.statusid == STATUS_ARRIVED)
                text += QString("<span style=\" color:#d1d1d1;\"><li><i>%1 x %2</i></li></span>").arg(itm.qty).arg(itm.name);
            else
                text += QString("<li><i>%1 x %2</i></li>").arg(itm.qty).arg(itm.name);
        }
        text += "</ul>";
    }
    text += "</ul>";
    ui->lblOrderDetails->setText(text);

    delete myDb;
}


void MainWindow::showPageDetails()
{
    ui->lblStartTime->setText(" -- ");
    ui->lblElapsedTime->setText(" -- ");
    ui->lblSOPicked->setText(" -- ");

    //Hey, it would be cool to slide the page..
    ui->frames->setCurrentIndex(1);

    //get data to show
    LCPrintOrderInfo order;
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    order = myDb->getPrintOrderInfo(orderId);

    updateOrderDetails();

    //Getting the different destinations
    QList<GeoDataCoordinates> twoPoints;
    viaPoints.clear();
    twoPoints.clear();

    /** @todo: get Actual COURIER POSITON (from gpsd)... **/
    qreal currLon=-118.11813;
    qreal currLat = 34.01814;
    GeoDataCoordinates currPosition( currLon, currLat, 0.0, GeoDataCoordinates::Degree );

    twoPoints.append(currPosition); //insert the currentPosition to the finalPoints.
    qDebug()<<"CurrentPosition:"<<currPosition.toString();
    int   zoom     = 2500;
    theMap->setHome(currLon, currLat, zoom);
    QTimer::singleShot(1000, theMap, SLOT(goHome()));

    //get client position.
    QString clientLon = order.clientPos.split(",").first();
    QString clientLat = order.clientPos.split(",").last();
    qDebug()<<"Client Position:"<<order.clientPos;
    clientPosition = GeoDataCoordinates(clientLon.toDouble() , clientLat.toDouble(), 0.0, GeoDataCoordinates::Degree );

    twoPoints.append(clientPosition);

    foreach(LCSubOrderInfo soi, order.subordersList ) {
        // Check the suborder status to see if we are re-taking this order (orphaned)
        if (soi.statusid == STATUS_ONTHEWAY) {
            qDebug()<<" ["<<soi.suborderid<<"] This sub order is already picked up. Ignoring the waypoint.";
        } else {
            //get each order position
            LCDestinationInfo  posInfo;
            QString orderLon = soi.pos.split(",").first();
            QString orderLat = soi.pos.split(",").last();
            GeoDataCoordinates pos(orderLon.toDouble() , orderLat.toDouble(), 0.0, GeoDataCoordinates::Degree );
            posInfo.gdCoordinate = pos;
            posInfo.name =  myDb->getProviderName(soi.providerid);
            qDebug()<<"SubOrder "<<soi.suborderid<<pos.toString()<<" SubOrder STATUS:"<<soi.statusid;
            viaPoints.append( posInfo );
        }
    }

    QApplication::setOverrideCursor(QCursor(Qt::BusyCursor));
    requestRoute(twoPoints); //Once the route has been calculated, then on the routeRetrieved() method add via points and get route again.

    delete myDb;
}


void MainWindow::routeReady(GeoDataDocument *gdDocument)
{
    qDebug()<<"Route Ready... Delaying 1000 ms";
    QTimer::singleShot(1000, this, SLOT(getWholeRoute()) );
}

void MainWindow::getWholeRoute()
{
    qDebug()<<"Getting the whole route...";
    RouteRequest * request = theMap->model()->routingManager()->routeRequest();
    qDebug()<<"ROUTE SIZE:"<<request->size();

    /** @note: Just ask the whole route if route retrieved size is 2. **/
    if ( request->size() != 2 ) {
        qDebug()<<"Route Request Finished.";
        QApplication::restoreOverrideCursor();
        return;
    }

    //Add via points to the route.
    LCPrintOrderInfo order;
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    order = myDb->getPrintOrderInfo(orderId);
    delete myDb;

    foreach( LCDestinationInfo point, viaPoints ) {
        request->addVia( point.gdCoordinate );

    }

    qDebug()<<"Updating route...";
    theMap->model()->routingManager()->updateRoute();

    //QTimer::singleShot(2000, this, SLOT(setPlaces()) );
    //theMap->addPlacemarkFile("/home/miguel/proyectos/KML/demo.kml");
}

void MainWindow::setPlaces()
{
    QVector<GeoDataPlacemark *> *vector = new QVector<GeoDataPlacemark *>;
    foreach ( LCDestinationInfo point, viaPoints ) {
        GeoDataPlacemark *placemark = new GeoDataPlacemark( point.name );
        placemark->setCoordinate( GeoDataPoint( point.gdCoordinate ) );
        placemark->setDescription("Add a description!");
        GeoDataLookAt *lookAt = new GeoDataLookAt( theMap->lookAt() ) ;
        lookAt->setLatitude( point.gdCoordinate.latitude() );
        lookAt->setLongitude( point.gdCoordinate.longitude() );
        placemark->setLookAt( lookAt );
        vector->append( placemark );
    }

    MarblePlacemarkModel *placemarkModel = static_cast<MarblePlacemarkModel*>( theMap->placemarkModel() );
    placemarkModel->setPlacemarkContainer( vector );
}


void MainWindow::requestRoute(QList<GeoDataCoordinates> positions)
{
    if (positions.count() < 2) return; //not route a non-destination route!

    RouteRequest * request = theMap->model()->routingManager()->routeRequest();

    //Set the first profile, which is my profile if no other one exists. See method "setupMarble()" to see my profile settings.
    request->setRoutingProfile( theMap->model()->routingManager()->profilesModel()->profiles().at(0) );

    for (int i=0; i <request->size(); ++i) {
        qDebug()<<"REMOVING request "<<i;
        request->remove(0);
    }

    int requestSize = request->size();
    for (int i=0; i < positions.count(); ++i) {
        qDebug()<<" adding position #"<<i;
        if ( i < requestSize ) {
            request->setPosition(i, positions.at(i)); //replace the existent null (or not) default ones.
            qDebug()<<" Reusing a position in the request";
        }
        else {
            request->append(positions.at(i)); //no space, insert a new one
            qDebug()<<" Appending a new position";
        }
    }

    /** request->setName(0, "INICIO");   still dont know what is the name for. Not displayed instead of (A). (A) is a QPixmap...
        we can obtain the pixmap pointer and modify it, yes?
        @todo: We can make a new QPixmap with the name of the restaurant!, need to modify marblelib **/
        //request->setName();

    theMap->model()->routingManager()->updateRoute();
}

void MainWindow::quit()
{
    qApp->exit();
}

void MainWindow::closeEvent(QCloseEvent *event)
 {
     QSettings settings("lemonCourier_courier", "lemonPOS Courier");
     settings.setValue("geometry", saveGeometry());
     settings.setValue("windowState", saveState());
     QMainWindow::closeEvent(event);
 }

MainWindow::~MainWindow()
{
    delete ui;
    delete groupPanel;
    delete readyPanel;
    delete printPanel;
    delete undeliveredPanel;
    delete errorMsg;

    delete pingTimer;

    if (modelsCreated) {
        delete subordersModel;
        delete ordersModel;
        delete interruptionsModel;
    }
    theMap->blockSignals(true);
    theRoutingWidget->blockSignals(true);
    theMap->model()->routingManager()->routeRequest()->clear(); //this seems to fix the hangup.

    RouteRequest * request = theMap->model()->routingManager()->routeRequest();
    for (int i=0; i <request->size(); ++i) {
        qDebug()<<"REMOVING request "<<i;
        request->remove(0);
    }

  //  CALLGRIND_DUMP_STATS;
}
