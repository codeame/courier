/**************************************************************************
*   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
*   miguel@lemonpos.org                                                   *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
***************************************************************************/
#include <QtGui>
#include <QtSql>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QLocale>
#include <QPixmap>

//#include <valgrind/callgrind.h>

#include "orderdelegate.h"
#include "../azahar.h"
#include "../structs.h"
#include "../misc.h"

QSize OrderDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  if(option.rect.width() == 0 || option.rect.height() == 0) return QSize(0, 0);

  //to get the size according to the font size and style
  QFont font = QApplication::font(); //QFont("Ubuntu", 9);
  font.setPointSize(9);
  font.setBold(true);
  QFontMetrics fontMetrics(font);

  //NOTE: in order to get the height needed, we need to know how many sub-orders the order has
  //      then to multiply that number by the font height. Unfortunately this implies a database query.
  LCPrintOrderInfo orderInfo;
  Azahar *myDb = new Azahar();
  qulonglong  orderId = index.data(Qt::DisplayRole).toULongLong();//model->data(indx, Qt::DisplayRole).toULongLong();
  myDb->setDatabase(db);
  orderInfo = myDb->getPrintOrderInfo(orderId);
  double lineCount = orderInfo.subordersList.count() + 3 + 0.5;
  delete myDb;

  return QSize(option.rect.width(), lineCount * fontMetrics.height());
}

void OrderDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (option.state & QStyle::State_Selected)
                 painter->fillRect(option.rect, option.palette.highlight());

   // CALLGRIND_START_INSTRUMENTATION;
    //get item data
    qulonglong  orderId = index.data(Qt::DisplayRole).toULongLong(); //model->data(indx, Qt::DisplayRole).toULongLong();

    LCPrintOrderInfo orderInfo;
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    orderInfo = myDb->getPrintOrderInfo(orderId);
    int margin = 1;

    //check if the status is INTERRUPTED.
    if ( orderInfo.statusid > STATUS_INTERRUPTED ) {
        int x = option.rect.x() + option.rect.width() - 35;
        int y = option.rect.y() + option.rect.height() - 35;
        QPixmap pix;
        QString imgPath = Misc::getAppDataPath()+"images/";

        switch ( orderInfo.statusid )
        {
            case STATUS_ORPHAN :
                pix = QPixmap(imgPath + "rescueme.png");
                break;
            case STATUS_ORPHAN_WARNING:
                pix = QPixmap(imgPath + "important.png");
                break;
            default:
                pix = QPixmap(imgPath + "green-asterisk.png");
        }

        painter->drawPixmap(x,y, 32, 32, pix);
    }

    //Painting OrderNo,ClientName,Date.
    QFont font = QApplication::font(); //QFont("Ubuntu", 9);
    font.setPointSize(9);
    font.setBold(true);
    painter->setFont(font);
    QFontMetrics fm(font);
    QString text = tr("Order No. ") + QString::number(orderInfo.orderid) +" "+ orderInfo.clientName;
    painter->drawText(option.rect.x()+2+margin,option.rect.y()+fm.height() , text); //at line 1
    //paint the address
    font.setBold(false);
    font.setItalic(true);
    painter->setFont(font);
    text = orderInfo.clientAddr;
    fm = painter->fontMetrics();
    painter->drawText(option.rect.x()+2+margin,option.rect.y()+fm.height()*2 , text); //at line 2
    //paint the ETA...
    font.setBold(false);
    font.setItalic(false);
    painter->setFont(font);
    //number of restaurants
    text = tr("%1 Sub Orders from:").arg(orderInfo.subordersList.count());
    painter->drawText(option.rect.x()+2+margin,option.rect.y()+fm.height()*3 , text); //at line 3
    //iterate each restaurant.
    int i=0;
    font.setItalic(true);
    painter->setFont(font);
    //NOTE: the ETA should be updated by dispatchApp. And re-think the ETA thing.
    //NOTE: the next code is not needed, because this will never be reached. Because when ONE suborder is picked, the order is in STATUS_PICKCONFIRMED or STATUS_ONTHEWAY
    foreach(LCSubOrderInfo soi, orderInfo.subordersList ) {
        if (soi.statusid == STATUS_ONTHEWAY || soi.statusid == STATUS_ARRIVED)
            text = tr("   %1, -picked-").arg( myDb->getProviderName(soi.providerid));
        else {
            if (soi.eta <= 0)
                text = tr("   %1, ready.").arg( myDb->getProviderName(soi.providerid));
            else
                text = tr("   %1, ready in %2 min.").arg( myDb->getProviderName(soi.providerid)).arg(soi.eta);
        }
        painter->drawText(option.rect.x()+2+margin,option.rect.y()+fm.height()*(4+i) , text); //at line 4,5..
        i++;
    }

    delete myDb;
    //CALLGRIND_STOP_INSTRUMENTATION;
}
