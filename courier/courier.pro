#-------------------------------------------------
#
# Project created by QtCreator 2010-09-06T11:37:10
#
#-------------------------------------------------
# Este LD_LIBRARY_PATH estaba en el build environment de courier y por eso no cargaba los plugins de los drivers de las bd.
# No se cuando se pudo haber cambiado, porque al principio si los cargaba.
# /home/miguel/qtcreator-2.0.0/lib:/home/miguel/qtcreator-2.0.0/lib/qtcreator:


QT       += core gui svg sql
#dbus

#LIBS += -L/usr/lib/marble -lmarblewidget
LIBS += -L/usr/lib -lmarblewidget

#INCLUDEPATH += /usr/local/include

TARGET = courierApp
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
        ../mibitWidgets/mibittip.cpp \
        ../mibitWidgets/mibitfloatpanel.cpp \
        ../mibitWidgets/mibitdialog.cpp \
        ../mibitWidgets/mibitnotifier.cpp \
        ../printing/print-dev.cpp \
        ../azahar.cpp \
        ../misc.cpp \
        orderdelegate.cpp


HEADERS  += mainwindow.h \
         ../mibitWidgets/mibittip.h \
         ../mibitWidgets/mibitfloatpanel.h \
         ../mibitWidgets/mibitdialog.h \
         ../mibitWidgets/mibitnotifier.h \
         ../structs.h \
         ../printing/print-dev.h \
         ../azahar.h \
         ../misc.h \
         orderdelegate.h

FORMS    += mainwindow.ui


TRANSLATIONS    = courierApp_es.ts \


#target.path = $$[QT_INSTALL_EXAMPLES]/sql/tablemodel

#INSTALLS += target
