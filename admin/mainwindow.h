/***************************************************************************
 *   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
 *   miguel@lemonpos.org                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlRelationalTableModel>
#include <QSqlTableModel>
#include <QDateTime>

#include "../structs.h"

#define QT_USE_FAST_CONCATENATION
#define QT_USE_FAST_OPERATOR_PLUS

class MibitFloatPanel;
class MibitNotifier;


namespace Ui {
    class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    ///Shows configuration panel
    void showDBConfig();
    void showUserConfig();
    ///Saves/Reads configuration information.
    void writeSettings();
    void readSettings();
    //quit
    void quit();
    //React when settings changed
    void settingsDBChanged();
    void setupDB();
    void connectToDb();
    void refreshModels();
    void showOrderDetails(const QModelIndex &index);
    void updateOrderDetails();
    void updateUserName();
    void displayError(const QString &msg);
    void authenticate();

    void resumeOperations();

private:
    Ui::MainWindow *ui;
    MibitFloatPanel *configPanel, *userPanel;
    MibitNotifier  *errorMsg;
    QSqlDatabase db;
    QSqlTableModel *ordersModel;
    bool   modelsCreated;
    qulonglong orderId;
    QTimer *updateTimer; //updates models and order details

    QString userId;
    QString userKey;
    bool    userAuthenticated;

    void setupModel();

protected:
     void closeEvent(QCloseEvent *event);

 //signals:

};

#endif // MAINWINDOW_H
