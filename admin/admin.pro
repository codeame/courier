#-------------------------------------------------
#
# Project created by QtCreator 2010-09-06T11:37:10
#
#-------------------------------------------------
# Este LD_LIBRARY_PATH estaba en el build environment de courier y por eso no cargaba los plugins de los drivers de las bd.
# No se cuando se pudo haber cambiado, porque al principio si los cargaba.
# /home/miguel/qtcreator-2.0.0/lib:/home/miguel/qtcreator-2.0.0/lib/qtcreator:


QT       += core gui svg sql

TARGET = courierAdmin
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
        ../mibitWidgets/mibitfloatpanel.cpp \
        ../mibitWidgets/mibitnotifier.cpp \
        ../azahar.cpp \
        ../misc.cpp \
        ../courier/orderdelegate.cpp


HEADERS  += mainwindow.h \
         ../mibitWidgets/mibitfloatpanel.h \
         ../mibitWidgets/mibitnotifier.h \
         ../structs.h \
         ../azahar.h \
         ../misc.h \
         ../courier/orderdelegate.h

FORMS    += mainwindow.ui


TRANSLATIONS    = courierAdmin_es.ts \


#target.path = $$[QT_INSTALL_EXAMPLES]/sql/tablemodel
#INSTALLS += target
