/***************************************************************************
 *   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
 *   miguel@lemonpos.org                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#include <QApplication>
#include <QLocale>
#include <QDebug>
#include <QTimer>
#include <QTextCodec>
#include <QtSql>
#include <QSettings>
#include <QIntValidator>
#include <QSplitter>


//#include <valgrind/callgrind.h>

#include "../structs.h"
#include "../misc.h"
#include "../courier/orderdelegate.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../azahar.h"
#include "../mibitWidgets/mibitfloatpanel.h"
#include "../mibitWidgets/mibitnotifier.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //fullscreen
    //setWindowState( windowState() | Qt::WindowFullScreen ); // set
    //fullscreen OFF
    //setWindowState( windowState() & ~Qt::WindowFullScreen ); // reset

    qDebug()<<tr("Starting courierAdmin...");
    readSettings();
    modelsCreated=false;
    userAuthenticated = false;
    userId = "";
    userKey= "";

    QString imgPath = Misc::getAppDataPath()+"images/";
    QString icoPath = Misc::getAppIconPath();

    //set Icon.
    setWindowIcon( QIcon(icoPath+"courierAdmin.png") );

    //Assing controls to panels.
    configPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top);
    configPanel->setSize(311,250);
    configPanel->addWidget(ui->groupConfig);
    configPanel->setMode(pmManual);
    configPanel->setHiddenTotally(true);
    configPanel->hide();

    userPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top );
    userPanel->setSize(250,250);
    userPanel->addWidget(ui->userConfig);
    userPanel->setMode(pmManual);
    userPanel->setHiddenTotally(true);
    userPanel->hide();

    QPixmap iconError = QPixmap( imgPath+"dialog-error.png");
    errorMsg = new MibitNotifier(this, imgPath+"rotated_panel_top.svg", iconError , false);
    errorMsg->setSize(300, 110); //ui->tabWidget->width()-25
    errorMsg->hide();

    connect( ui->btnSaveConfig, SIGNAL(clicked()), configPanel, SLOT(hidePanel()) );
    connect( ui->btnSaveConfig, SIGNAL(clicked()), this, SLOT(writeSettings()) );
    connect( ui->btnSaveConfig, SIGNAL(clicked()), this, SLOT(settingsDBChanged()));
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), this, SLOT(writeSettings()));
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), userPanel, SLOT(hidePanel()) );
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), this, SLOT( updateUserName() ) );
    connect( ui->actionShow_Settings, SIGNAL(triggered()), this, SLOT(showDBConfig()));
    connect( ui->actionShow_Users_Settings, SIGNAL(triggered()), this, SLOT(showUserConfig()));
    connect( ui->actionExit, SIGNAL(triggered()), this, SLOT(quit()));
    connect(ui->orderList, SIGNAL(clicked(const QModelIndex &)), SLOT(showOrderDetails(const QModelIndex &)) );


    QTimer::singleShot(1000, this, SLOT(setupDB()));
    updateTimer = new QTimer(this);
    updateTimer->setInterval(3000);
    connect(updateTimer, SIGNAL(timeout()), SLOT(refreshModels()) );
    connect(updateTimer, SIGNAL(timeout()), SLOT(updateOrderDetails()) );
    updateTimer->start();

    statusBar()->showMessage(tr("Welcome to CourierAdmin. (C) 2010, Miguel Chavez Gamboa"));
    ui->lblUserName->setText( tr("User #%1").arg( ui->editCfgUserId->text() ) );

    ui->tabWidget->setDisabled(true); // disable while not authenticated.
    QTimer::singleShot(1000, this, SLOT(authenticate()));
    ui->actionResume->setDisabled(true);
    connect(ui->actionResume, SIGNAL(triggered()), SLOT( resumeOperations() )  );
}


void MainWindow::authenticate()
{
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    LCUserInfo uInfo = myDb->getUserInfo( ui->editCfgUserId->text().toULongLong() );
    qDebug()<<" USER ID:"<<ui->editCfgUserId->text().toULongLong()<<" got:"<<uInfo.id;

    if (uInfo.key == ui->editCfgUserKey->text()) {
        //Yes, they are the same.
        userAuthenticated = true;
        ui->tabWidget->setEnabled(true);
        userId  = ui->editCfgUserId->text();
        userKey = ui->editCfgUserKey->text();
        qDebug()<<"** Authenticating :: PASSED **";
    } else {
        //NO, they are not the same key.
        userAuthenticated = false;
        ui->tabWidget->setDisabled(true);
        userId  = "";
        userKey = "";
        displayError(tr("Error, the user key is invalid!"));
        qDebug()<<"** Authenticating :: FAILED **";
        //show the config panel
        showUserConfig();
        ui->editCfgUserKey->setFocus();
        ui->editCfgUserKey->setSelection(0, ui->editCfgUserKey->text().size()-1);
    }
}


void MainWindow::resumeOperations()
{
    ui->actionResume->setDisabled(true);
    ui->tabWidget->setEnabled(true);
}


void MainWindow::displayError(const QString &msg)
{
    //launch the panel.
    errorMsg->setSize(ui->tabWidget->width()-50, 100);
    errorMsg->setTextColor("#ff5500");
    errorMsg->showNotification(msg);
}


void MainWindow::updateUserName()
{
    if (db.isOpen()) {
        Azahar *myDb = new Azahar();
        myDb->setDatabase(db);
        LCUserInfo uInfo;
        //get user name
        uInfo = myDb->getUserInfo( ui->editCfgUserId->text().toULongLong() );
        if (uInfo.key == ui->editCfgUserKey->text())
            ui->lblUserName->setText( tr("User: %1").arg(uInfo.name) );
        else
            ui->lblUserName->setText( tr("User: Not Authenticated") );
    } else {
        ui->lblUserName->setText( tr("User #%1").arg( ui->editCfgUserId->text() ) );
    }
}

void MainWindow::showDBConfig()
{
    //load config
    readSettings();
    //show it...
    configPanel->showPanel();
    //set focus to the db field
    ui->editCfgDatabase->setFocus();
}

void MainWindow::showUserConfig()
{
    //load config
    readSettings();
    //show it...
    userPanel->showPanel();
    ui->editCfgUserId->setFocus();
}

void MainWindow::readSettings()
 {
     QSettings settings("lemonCourier_admin", "lemonPOS Courier");
     ui->editCfgDatabase->setText(settings.value("dbName", QString("lemoncourier")).toString());
     ui->editCfgHost->setText(settings.value("dbHost", QString("localhost")).toString());
     ui->editCfgPassword->setText(settings.value("dbPassword", QString("")).toString());
     ui->editCfgProvider->setCurrentIndex( ui->editCfgProvider->findText( (settings.value("dbProvider", QString("PostgreSQL")).toString()) , Qt::MatchExactly ) );
     ui->editCfgUser->setText(settings.value("dbUser", QString("lemonuser")).toString());

     ui->editCfgUserId->setText(settings.value("userID", QString("PLEASE ENTER YOUR ID")).toString());
     /** @todo @note: Hey, allow to save USER password (key) ? **/
     ui->editCfgUserKey->setText(settings.value("userKEY", QString("PLEASE ENTER YOUR KEY")).toString());

     restoreGeometry(settings.value("geometry").toByteArray());
     restoreState(settings.value("windowState").toByteArray());
 }

void MainWindow::writeSettings()
 {
     QSettings settings("lemonCourier_admin", "lemonPOS Courier");
     settings.setValue("dbName", ui->editCfgDatabase->text());
     settings.setValue("dbHost", ui->editCfgHost->text());
     settings.setValue("dbPassword", ui->editCfgPassword->text());
     settings.setValue("dbProvider", ui->editCfgProvider->currentText());
     settings.setValue("dbUser", ui->editCfgUser->text());

     settings.setValue("userID", ui->editCfgUserId->text().toULongLong());
     settings.setValue("userKey", ui->editCfgUserKey->text());

     if ( userId != ui->editCfgUserId->text() || userKey != ui->editCfgUserKey->text() || !userAuthenticated ) {
         // authenticate again, because courierId or courierKey has changed or not authenticated yet.
         qDebug()<<"WriteSettings :: Authenticating...";
         authenticate();
     }

 }

/* NOTE: Config needs a keyboard!!!.. or use a virtual one.
   Once is configured, its not needed, so its no need to make available the config dialog  */
void MainWindow::settingsDBChanged()
{
  if (db.isOpen()) db.close();

  QString toCompare;
  qDebug()<<" Database STR ON COMBO:"<<ui->editCfgProvider->currentText()<<" Database still in use:"<<db.driverName();
  if (ui->editCfgProvider->currentText() == "MySQL") toCompare = "QMYSQL"; else toCompare = "QPSQL";

  if (toCompare != db.driverName() ) {
      qDebug()<<"Database driver was changed...  new driver:"<<db.driverName();
      if (modelsCreated) {
        delete ordersModel;
      }
      db.close();
      db.removeDatabase(db.connectionName());

      QString dbName = "";
      if (ui->editCfgProvider->currentText() == "MySQL")
          dbName = "QMYSQL";
      else //for now only mysql and psql
          dbName = "QPSQL";

      qDebug()<<"Adding database...";
      db = QSqlDatabase::addDatabase(dbName);
      qDebug()<<"Database Added: "<<db;

      // QSqlTableModels need to be created after adding the database ( db = addDatabase... )
      ordersModel     = new QSqlTableModel();
      modelsCreated   = true;

      db.setHostName(ui->editCfgHost->text());
      db.setDatabaseName(ui->editCfgDatabase->text());
      db.setUserName(ui->editCfgUser->text());
      db.setPassword(ui->editCfgPassword->text());
      connectToDb();
  } else {
      return;
  }
}

void MainWindow::setupDB()
{
  qDebug()<<"Setting up database...";
  if (db.isOpen()) db.close();
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

  QString dbName = "";
  if (ui->editCfgProvider->currentText() == "MySQL")
      dbName = "QMYSQL";
  else //for now only mysql and psql
      dbName = "QPSQL";

  qDebug()<<"DB DRIVERS:"<<db.drivers();
  qDebug()<<"Adding the new Database...";
  db = QSqlDatabase::addDatabase(dbName);
  db.setHostName(ui->editCfgHost->text());
  db.setDatabaseName(ui->editCfgDatabase->text());
  db.setUserName(ui->editCfgUser->text());
  db.setPassword(ui->editCfgPassword->text());

  connectToDb();
}

void MainWindow::connectToDb()
{
  if (!db.open()) {
    db.open(); //try to open connection
    qDebug()<<"(1/connectToDb) Trying to open connection to database..";
  }
  if (!db.isOpen()) {
    db.open(); //try to open connection again...
    qDebug()<<"(2/connectToDb) Trying to open connection to database..";
  }
  if (!db.isOpen()) {
    qDebug()<<"(3/connectToDb) Configuring..";
    configPanel->showPanel();
  } else {
    qDebug()<<" CONNECTED. ";
    if (!modelsCreated) { //Create models...
      qDebug()<<"Creating model...";
      ordersModel     = new QSqlTableModel();
      modelsCreated   = true;
    }
    qDebug()<<"Calling setupModel()";
    setupModel();
  }
}


void MainWindow::setupModel()
{
  qDebug()<<"SetupModel...";
  if (!db.isOpen()) {
    connectToDb();
  }
  else {
    qDebug()<<"setting up database model";
    ordersModel->setTable(ORDERS_TABLE);

    if (ordersModel->tableName() != ORDERS_TABLE)
      ordersModel->setTable(ORDERS_TABLE);

    ordersModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ordersModel->setHeaderData(ordersModel->fieldIndex(OC_ID), Qt::Horizontal, tr("Order"));
    ordersModel->setHeaderData(ordersModel->fieldIndex(OC_CLIENTID), Qt::Horizontal, tr("Client"));

    /** @todo: take this code to the appropiate method when selecting the provider/courier message. **/
    //ui->providersList->setModel(subordersModel);
    //ui->providersList->setModelColumn(subordersModel->fieldIndex(SOC_PROVID));
    //ui->providersList->setEditTriggers(QAbstractItemView::NoEditTriggers);

    /** @todo: Create another orders delegate, to show even MORE details, like status REASONS. **/
    ui->orderList->setModel(ordersModel);
    ui->orderList->setModelColumn(ordersModel->fieldIndex("id"));
    ui->orderList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    OrderDelegate *oDelegate = new OrderDelegate(ui->orderList, db);
    ui->orderList->setItemDelegate(oDelegate);

    /** @todo: Set the default sort.. to display all orders **/
    ordersModel->setSort( ordersModel->fieldIndex(OC_STATUSID),Qt::DescendingOrder );
    updateUserName();
    refreshModels();
  }
}

void MainWindow::refreshModels()
{
    if ( modelsCreated && db.isOpen() ) {

        //check for orphan orders
        Azahar *myDb = new Azahar();
        myDb->setDatabase(db);

        /** @todo: REVISE THIS FILTER **/

        //if ( !myDb->getOrphanOrders().isEmpty() ) {
        //    ordersModel->setFilter( QString("%1.%2>=%3 AND %1.%4=%5") /** @note: REVISE IF ADDED MORE STATUS > 15 **/
        //                            .arg(ORDERS_TABLE).arg(OC_STATUSID)
        //                            .arg(STATUS_ORPHAN_WARNING).arg(OC_COURIERID).arg(userId));
        //} else {
            ordersModel->setFilter( QString("%1.%2=%3 OR %1.%2=%4 OR (%1.%2>%5 AND %1.%2<=%6)")
                                .arg(ORDERS_TABLE).arg(OC_STATUSID)
                                .arg(STATUS_READY).arg(STATUS_CONFIRMED)
                                .arg(STATUS_INTERRUPTED).arg(REASON_INT_TRAFFICEME));
        //}
        ordersModel->select();
    }
}


void MainWindow::showOrderDetails(const QModelIndex &index)
{
    qulonglong row = index.row();
    QModelIndex indx = ordersModel->index(row, ordersModel->fieldIndex(OC_ID));
    orderId = ordersModel->data(indx, Qt::DisplayRole).toULongLong();
    updateOrderDetails();
    ui->groupDetails->show();
}

void MainWindow::updateOrderDetails()
{
    //get data to show
    LCPrintOrderInfo order;
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    order = myDb->getPrintOrderInfo(orderId);

    if (order.orderid <= 0) {
        ui->groupDetails->hide();
        return;
    }

    /** @todo: show the order details label... hide when not selected an order. **/

    //writing the order details text
    QString text = QString("<b><span style=\" font-size:13pt;\">%1 %2 - %3 </span></b><br> &nbsp;&nbsp;&nbsp;&nbsp;<i>%4</i><br>&nbsp;&nbsp;&nbsp;&nbsp;%5 %6 <ul>")
                   .arg(tr("Order No.")).arg(order.orderid).arg(order.clientName)
                   .arg(order.clientAddr)
                   .arg(order.subordersList.count())
                   .arg(tr("Sub Orders From:"));
    //get each sub order details.
    foreach(LCSubOrderInfo soi, order.subordersList ) {
        if (soi.statusid == STATUS_ONTHEWAY || soi.statusid == STATUS_ARRIVED) {
            text += QString("<li><span style=\"text-decoration: line-through;\"><b>%1</b><span style=\" color:#ff5500;\"><i> Picked </i></span>.</span></li> <ul> ")
                    .arg( myDb->getProviderName(soi.providerid));
        }
        else if ( (soi.statusid == STATUS_READY || soi.statusid == STATUS_PICKCONFIRMED) && soi.eta <= 0) {
            text += QString("<li><b>%1</b>,<span style=\" color:#ff5500;\"><i> %2 </i></span>.</span></li> <ul> ")
                .arg( myDb->getProviderName(soi.providerid))
                .arg(tr("ready"));
        }
        else {
            text += QString("<li><b>%1</b>, %2 <span style=\" color:#ff5500;\"><i>%3 min</i></span>.</span></li> <ul> ")
                .arg( myDb->getProviderName(soi.providerid))
                .arg(tr("ready in"))
                .arg(soi.eta);
        }
        //each suborder element
        foreach(LCPrintOrderItems itm, soi.items ) {
            if (soi.statusid == STATUS_ONTHEWAY || soi.statusid == STATUS_ARRIVED)
                text += QString("<span style=\" color:#d1d1d1;\"><li><i>%1 x %2</i></li></span>").arg(itm.qty).arg(itm.name);
            else
                text += QString("<li><i>%1 x %2</i></li>").arg(itm.qty).arg(itm.name);
        }
        text += "</ul>";
    }
    text += "</ul>";
    ui->lblOrderDetails->setText(text);

    delete myDb;
}

void MainWindow::quit()
{
    qApp->exit();
}

void MainWindow::closeEvent(QCloseEvent *event)
 {
     QSettings settings("lemonCourier_admin", "lemonPOS Courier");
     settings.setValue("geometry", saveGeometry());
     settings.setValue("windowState", saveState());
     QMainWindow::closeEvent(event);
 }

MainWindow::~MainWindow()
{
    delete ui;
    delete configPanel;
    delete userPanel;
    delete errorMsg;

    delete updateTimer;

    if (modelsCreated) {
        delete ordersModel;
    }
  //  CALLGRIND_DUMP_STATS;
}
