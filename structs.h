/***************************************************************************
 *   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
 *   miguel@lemonpos.org                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#ifndef MYSTRUCTS_H
#define MYSTRUCTS_H

#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QPixmap>
#include <QDateTime>
#include <QDate>
#include <QHash>

#include <marble/GeoDataCoordinates.h>


#define STATUS_UNKNOWN       0
#define STATUS_AUTHORIZED    1   // Paid order.
#define STATUS_UNCONFIRMED   2   // Not confirmed (time) by the restaurant.
#define STATUS_CONFIRMED     3   // Confirmed (time) by the restaurant.
#define STATUS_READY         4   // Ready to pickup by the courier.
#define STATUS_PICKCONFIRMED 5   // The courier confirmed to pick up the order at the restaurant.
#define STATUS_ONTHEWAY      6   // Picked up and on the way to the client.
#define STATUS_ARRIVED       7   // Order has arrived to the client address, awaiting to be received.
#define STATUS_DELIVERED     8   // The order has been delivered to the client. ORDER CLOSED.
#define STATUS_UNDELIVERED   9   // The order could not be delivered due to client's reasons. (inexistent address, client abscence, rejected)

#define STATUS_INTERRUPTED    9
#define REASON_INT_EMERGENCY  10 // the driver feels bad, sick... or injured, or robbed! (robbed, cannot finish the delivery, re-ordering)
#define REASON_INT_TRAFFICJAM 11 // this is just a pause, due to traffic jam... other courier could not help.
#define REASON_INT_CARFAILURE 12
#define REASON_INT_TRAFFICEME 13 // Traffic jam unsolved for long time, rescueme in motorcycle or walking
#define STATUS_ORPHAN_WARNING 14
#define STATUS_ORPHAN         15


//Next status are for undelivered orders. For easy reason selection.
#define REASON_INEXISTENTADD 14
#define REASON_CLIENTABSENCE 15
#define REASON_REJECTED      16
#define REASON_INCOMPLETE    17
#define REASON_TOOLATE       18


//TABLE NAMES
#define SUBORDERS_TABLE  "sub_orders"
#define SUBORDERS_VIEWC  "v_csuborders"
#define ORDERITEMS_TABLE "order_items"
#define SUBORDERS_VIEWR  "v_rsuborders"
#define ORDERS_TABLE     "orders"
#define PROVIDERS_TABLE  "providers"
#define INTERRUPTIONS_TABLE "interruption_reasons"
#define INTERRUPTIONS_ACTIONS "interruption_actions"

//COLUM NAMES

//Interruptions
#define INT_ID "id"
#define INT_NAME "name"
#define INT_ACT_ID "id"
#define INT_ACT_NAME "name"

//Providers Columns
#define PRO_ID "id"
#define PRO_NAME "name"

//Users
#define USR_TABLE "users"
#define USR_ID "id"
#define USR_NAME "name"
#define USR_KEY "key"
#define USR_SALT "salt"
#define USR_LASTLOGIN "lastlogin"

//orders Columns
#define OC_ID "id"
#define OC_CLIENTID "clientid_id"
#define OC_COURIERID "courierid_id"
#define OC_DATETIME "datetime"
#define OC_POSITION "position"
#define OC_STATUSID "status_id"
#define OC_SUBTOTAL "subtotal"
#define OC_TOTALTAX "totaltax"
#define OC_ETA      "eta"
#define OC_PING     "ping"

//suborders  Columns
#define SOC_ID "id"
#define SOC_ORDERID "orderid_id"
#define SOC_PROVID "providerid_id"
#define SOC_STATUSID "status_id"
#define SOC_LABEL "label"
#define SOC_COMMENTS "comments"
#define SOC_ETA "eta"

//order items columns
#define OIC_ID "id"
#define OIC_ORDERID "orderid_id"
#define OIC_SUBORDERID "suborderid_id"
#define OIC_PROVID "providerid_id"
#define OIC_PRODUCTID "productid_id"
#define OIC_QTY "qty"
#define OIC_PRICE "price"
#define OIC_NAME "name"
#define OIC_TAX "tax"


struct LCPrintOrderItems
{
    double  qty;
    QString name;
};

struct LCSubOrderInfo
{
    qulonglong orderid; //the order to which it belongs to.
    qulonglong suborderid;
    qulonglong providerid;
    qulonglong statusid;
    QString label;
    QString comments;
    int     eta;
    QList<LCPrintOrderItems> items;
    QString pos;
};

struct LCPrintOrderInfo
{
  qulonglong orderid;
  qulonglong statusid;
  qulonglong courierid;
  QString    clientName;
  QString    clientAddr;
  int        timeToPick;
  QList<LCSubOrderInfo> subordersList;
  QString    clientPos;
};

struct LCClientInfo
{
    qulonglong id;
    QString firstname;
    QString lastname;
    QString company;
    QString email;
    QString password;
    QString phone;
    QString addr1;
    QString addr2;
    QString city;
    QString state;
    QString zip;
    QDate   since;
    QString pos;
    QString lastip;
    bool    emailme;
};

struct LCProviderInfo
{
    qulonglong id;
    QString name;
    QString pos;
};

struct LCCourierInfo
{
    qulonglong id;
    QString key;
    QString name;
    QString phone;
    QString pos;
};

struct LCUserInfo
{
    qulonglong id;
    QString    name;
    QString    key;
    QString    salt;
    QDateTime  lastLogin;
};

struct LCDestinationInfo
{
    QString name;
    Marble::GeoDataCoordinates gdCoordinate;
};

#endif
