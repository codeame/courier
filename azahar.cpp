/**************************************************************************
*   Copyright (C) 2007-2009 by Miguel Chavez Gamboa                       *
*   miguel@lemonpos.org                                                   *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
***************************************************************************/

#include <QWidget>
#include <QByteArray>
#include "azahar.h"

Azahar::Azahar(QObject *parent) :
    QObject(parent)
{
    errorStr = "";
}


void Azahar::setDatabase(const QSqlDatabase& database)
{
  db = database;
  if (!db.isOpen()) db.open();
}

bool Azahar::isConnected()
{
  return db.isOpen();
}

void Azahar::setError(QString err)
{
  errorStr = err;
}

QString Azahar::lastError()
{
  return errorStr;
}


// Kitchen Order Stuff

LCPrintOrderInfo Azahar::getPrintOrderInfo(qulonglong oid)
{
    LCPrintOrderInfo info;
    info.orderid=0;
    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        qulonglong            clientId = getClientIdForOrder(oid);
        LCClientInfo          cInfo    = getClientInfo(clientId);
        QList<LCSubOrderInfo> suborders= getSubOrders(oid); //this contains each item for the suborder.

        info.clientPos  = cInfo.pos;
        info.clientAddr = QString("%1, %2, %3, %4, %5").arg(cInfo.addr1).arg(cInfo.addr2).arg(cInfo.city).arg(cInfo.state).arg(cInfo.zip);
        info.clientName = QString("%1, %2").arg(cInfo.lastname).arg(cInfo.firstname);
        info.statusid   = getOrderStatus(oid);
        info.subordersList = suborders;

        if ( orderExists(oid) )
            info.orderid    = oid;
        else
            info.orderid    = 0;

    } //if db is open

    return info;
}

bool Azahar::orderExists(const qulonglong &oid)
{
    bool result = false;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("SELECT * from %1 where %2=:soid;").arg(ORDERS_TABLE).arg(OC_ID));
        query.bindValue(":soid", oid);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING ORDER EXISTS:"<<query.lastError().text();
          } else {
              while (query.next()) {
                  result = true;
              }
          }
    }

    return result;
}

int Azahar::getOrderStatus(const qulonglong &oid)
{
    int result = 0;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("SELECT %1,%2 from %3 where %1=:soid;").arg(OC_ID).arg(OC_STATUSID).arg(ORDERS_TABLE));
        query.bindValue(":soid", oid);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING ORDER STATUS FOR THE SPECIFIED ORDER:"<<query.lastError().text();
          } else {
              while (query.next()) {
                  int fieldStatus = query.record().indexOf(OC_STATUSID);
                  result = query.value(fieldStatus).toInt();
              }
          }
    }

    return result;
}

LCSubOrderInfo Azahar::getPrintSubOrderInfo(qulonglong soid)
{
    LCSubOrderInfo info;
    info.orderid    = 0;
    info.suborderid = 0;
    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT * from sub_orders where id=:ID");
        query.bindValue(":ID", soid);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING SUBORDERINFO:"<<query.lastError().text();
          } else {
              while (query.next()) {
                int fieldOrderId = query.record().indexOf(SOC_ORDERID);
                int fieldLabel   = query.record().indexOf(SOC_LABEL);
                int fieldComments= query.record().indexOf(SOC_COMMENTS);
                int fieldSoid    = query.record().indexOf(SOC_ID);
                info.orderid    = query.value(fieldOrderId).toULongLong();
                info.label      = query.value(fieldLabel).toString();
                info.comments   = query.value(fieldComments).toString();
                info.suborderid = query.value(fieldSoid).toULongLong();
                //ITEMS
                info.items = getPrintOrderItems(soid);
              }
          }
    } //if db is open

    return info;
}

bool Azahar::setEachSubOrderStatus(const qulonglong &orderid, const int &status)
{
    // update transactions set providerid=1 where transactions.providerid NOT IN (SELECT providers.id from providers);
    bool result = false;
    if (!db.isOpen()) db.open();
      if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("UPDATE %1 SET %2=:status WHERE %3=:id").arg(SUBORDERS_TABLE).arg(SOC_STATUSID).arg(SOC_ORDERID));
        query.bindValue(":id", orderid);
        query.bindValue(":status", status); //this is a statusID from OrderStatus table, not the status itself (but can be the same).
        if (query.exec()) {
            result = true;
            qDebug()<<"Rows Affected by the (each) SUB_ORDERS status UPDATE:"<<query.numRowsAffected();
        }
        else {
          setError(query.lastError().text());
          qDebug()<<"ERROR: "<<query.lastError();
        }
      }
    return result;
}

bool Azahar::setSubOrderStatus(const qulonglong &id, const int &status)
{
    // update transactions set providerid=1 where transactions.providerid NOT IN (SELECT providers.id from providers);
    bool result = false;
    if (!db.isOpen()) db.open();
      if (db.isOpen()) {
        QSqlQuery query(db);
        if (status == STATUS_READY) { //on status_ready, reset the eta! TODO: re-think eta for other statuses
            query.prepare(QString("UPDATE %1 SET %2=:status, %4=:eta WHERE %3=:id").arg(SUBORDERS_TABLE).arg(SOC_STATUSID).arg(SOC_ID).arg(SOC_ETA));
            query.bindValue(":eta", 0);
        } else {
            query.prepare(QString("UPDATE %1 SET %2=:status WHERE %3=:id").arg(SUBORDERS_TABLE).arg(SOC_STATUSID).arg(SOC_ID));
        }
        query.bindValue(":id", id);
        query.bindValue(":status", status); //this is a statusID from OrderStatus table, not the status itself (but can be the same).
        if (query.exec()) {
            updateOrderStatus(id);
            result = true;
            qDebug()<<"Rows Affected by the SUB_ORDERS status UPDATE:"<<query.numRowsAffected();
        }
        else {
          setError(query.lastError().text());
          qDebug()<<"ERROR: "<<query.lastError();
        }
      }
    return result;
}

bool Azahar::setSubOrderETA(const qulonglong &id, const int &minutes)
{
    bool result = false;
    if (!db.isOpen()) db.open();
      if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("UPDATE %1 SET %2=:eta WHERE %3=:id").arg(SUBORDERS_TABLE).arg(SOC_ETA).arg(SOC_ID));
        query.bindValue(":eta", minutes);
        query.bindValue(":id", id);
        if (query.exec()) {
            result = true;
            qDebug()<<"Rows Affected by the SUBORDER ETA UPDATE:"<<query.numRowsAffected();
        }
        else {
            setError(query.lastError().text());
            qDebug()<<"ERROR: "<<query.lastError();
        }
      }
    return result;
}

bool Azahar::setOrderETA(const qulonglong &id, const int &minutes)
{
    bool result = false;
    if (!db.isOpen()) db.open();
      if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("UPDATE %1 SET %2=:eta WHERE %3=:id").arg(ORDERS_TABLE).arg(OC_ETA).arg(OC_ID));
        query.bindValue(":eta", minutes);
        query.bindValue(":id", id);
        if (query.exec()) {
            result = true;
            qDebug()<<"Rows Affected by the ORDER ETA UPDATE:"<<query.numRowsAffected();
        }
        else {
            setError(query.lastError().text());
            qDebug()<<"ERROR: "<<query.lastError();
        }
      }
    return result;
}

//also sets each suborder's status
bool Azahar::setOrderStatus(const qulonglong &id, const int &status, const qulonglong &courierid, const bool &updateSubOrders)
{
    bool result = false;
    if (!db.isOpen()) db.open();
      if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("UPDATE %1 SET %2=:status,%3=:courierId WHERE %4=:id").arg(ORDERS_TABLE).arg(OC_STATUSID).arg(OC_COURIERID).arg(OC_ID));
        query.bindValue(":courierId", courierid);
        query.bindValue(":id", id);
        query.bindValue(":status", status); //this is a statusID from OrderStatus table, not the status itself (but can be the same).
        if (query.exec()) {
            if (updateSubOrders)
                result =  setEachSubOrderStatus( id, status ); //result will depend on suborder status changes.
            else
                result = true;
            //qDebug()<<"Rows Affected by the ORDER status UPDATE:"<<query.numRowsAffected();
        }
        else {
          setError(query.lastError().text());
          qDebug()<<"ERROR: "<<query.lastError();
        }
      }
    return result;
}

//NOTE: -Caution- Do not call updateOrderStatus(suborder) because it will enter an endless loop!!!
bool Azahar::setOrderStatus(const qulonglong &id, const int &status)
{
    bool result = false;
    if (!db.isOpen()) db.open();
      if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("UPDATE %1 SET %2=:status WHERE %3=:id").arg(ORDERS_TABLE).arg(OC_STATUSID).arg(OC_ID));
        query.bindValue(":id", id);
        query.bindValue(":status", status); //this is a statusID from OrderStatus table, not the status itself (but can be the same).
        if (query.exec()) {
            result = true;
            //qDebug()<<"Rows Affected by the ORDER status UPDATE:"<<query.numRowsAffected();
        }
        else {
          setError(query.lastError().text());
          qDebug()<<"ERROR: "<<query.lastError();
        }
      }
    return result;
}

void Azahar::updateOrderStatus(const qulonglong &suborder )
{
    //first get the order_id of this suborder.
    qulonglong order_id = getSubOrderOrderId(suborder);
    if (order_id <= 0) return; //order not found! abort
    //now iterate through all the suborders...
    int confirmed = 0;
    int ready     = 0;
    QList<LCSubOrderInfo> sub_orders = getSubOrders(order_id);
    foreach(LCSubOrderInfo info, sub_orders ) {
        qDebug()<<"Inspecting suborder #"<<info.suborderid<<" With STATUS_ID:"<<info.statusid;
        if (info.statusid == STATUS_CONFIRMED)
            confirmed++;
        else if (info.statusid == STATUS_READY)
            ready++;
    }
    //TODO: how to set the order status by its suborder statuses? REFINE THIS CODE:
    if (sub_orders.count() == 0) return; //Hey, this should not occur, because we have the suborder which represents ONE suborder.
    int STATUS = 0;
    if (ready+confirmed == sub_orders.count()) {
        //all suborders are confirmed or ready, so can be shown on the courierApp.
        if (ready > confirmed)
            STATUS = STATUS_READY;
        else if ( ready < confirmed)
            STATUS = STATUS_CONFIRMED;
        else
            STATUS = STATUS_CONFIRMED; //what to set when are equal?
        //set the status...
        qDebug()<<"The order#"<<order_id<<" got STATUS_ID:"<<STATUS;
        setOrderStatus(order_id, STATUS);
    } //else, there are some suborders with other status (UNCONFIRMED, DELIVERED,PICKCONFIRMED, ONTHEWAY, etc..) which are not suposed to be shown.
}

qulonglong Azahar::getSubOrderOrderId(const qulonglong &soid)
{
    qulonglong result = 0;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("SELECT %1 from %2 where %3=:soid;").arg(SOC_ORDERID).arg(SUBORDERS_TABLE).arg(SOC_ID));
        query.bindValue(":soid", soid);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING ORDER_ID FOR THE SPECIFIED SUBORDER:"<<query.lastError().text();
          } else {
              while (query.next()) {
                  int fieldId = query.record().indexOf(SOC_ORDERID);
                  result = query.value(fieldId).toULongLong();
                  qDebug()<<"SUBORDER_ID requested:"<<soid<<" ORDER_ID BELONGING:"<<result;
              }
          }
    }
    return result;
}

QList<LCSubOrderInfo> Azahar::getSubOrders(qulonglong orderid)
{
    QList<LCSubOrderInfo> info;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT * from sub_orders where orderid_id=:orderID;");
        query.bindValue(":orderID", orderid);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING SUBORDERS FOR THE SPECIFIED ORDER:"<<query.lastError().text();
          } else {
              while (query.next()) {
                int fieldId = query.record().indexOf("id");
                int fieldOId = query.record().indexOf("orderid_id");
                int fieldPId = query.record().indexOf("providerid_id");
                int fieldSId = query.record().indexOf("status_id");
                int fieldLbl = query.record().indexOf("label");
                int fieldCom = query.record().indexOf("comments");
                int fieldEta = query.record().indexOf("eta");

                LCSubOrderInfo soInfo;
                soInfo.suborderid = query.value(fieldId).toULongLong();
                soInfo.orderid    = query.value(fieldOId).toULongLong();
                soInfo.providerid = query.value(fieldPId).toULongLong();
                soInfo.statusid   = query.value(fieldSId).toULongLong();
                soInfo.label      = query.value(fieldLbl).toString();
                soInfo.comments   = query.value(fieldCom).toString();
                soInfo.eta        = query.value(fieldEta).toInt();
                //get each item for the suborder...
                soInfo.items = getPrintOrderItems(soInfo.suborderid);

                //get provider position
                soInfo.pos = getProviderPosition(soInfo.providerid);
                info.append(soInfo);
                //qDebug()<<"Appending suborder "<<soInfo.suborderid<<" labeled as "<<soInfo.label;
            }
          }
      }

    return info;
}

qulonglong Azahar::getClientIdForOrder(qulonglong order)
{
    qulonglong id = 0;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT * from orders where id=:orderID;");
        query.bindValue(":orderID", order);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING CLIENT ID FOR THE SPECIFIED ORDER:"<<query.lastError().text();
          } else {
              //get query result
              while (query.next()) {
                int fieldId = query.record().indexOf("clientid_id");
                id = query.value(fieldId).toULongLong();
                //qDebug()<<"clientID:"<<id<<" for order "<<order;
            }
          }
      }

    return id;
}

int Azahar::getOrderPickedItemsCount(const qulonglong &order)
{
    int result = 0;
    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("SELECT id from %1 where %2=:orderID and %3=:status;").arg(SUBORDERS_TABLE).arg(SOC_ORDERID).arg(SOC_STATUSID));
        query.bindValue(":orderID", order);
        query.bindValue(":status", STATUS_ONTHEWAY);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING PICKED COUNT FOR THE SPECIFIED ORDER:"<<query.lastError().text();
          } else {
              //get the count
              result = query.size();
          }
      }
    return result;
}

int Azahar::getOrderItemsCount(const qulonglong &order)
{
    int result = 0;
    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("SELECT id from %1 where %2=:orderID;").arg(SUBORDERS_TABLE).arg(SOC_ORDERID));
        query.bindValue(":orderID", order);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING COUNT FOR THE SPECIFIED ORDER:"<<query.lastError().text();
          } else {
              //get the count
              result = query.size();
          }
      }
    return result;
}

QDateTime Azahar::getLastPing(const qulonglong &order)
{
    QDateTime result;
    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("SELECT id,ping from %1 where %2=:orderID;").arg(ORDERS_TABLE).arg(OC_ID));
        query.bindValue(":orderID", order);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING PING FOR THE SPECIFIED ORDER:"<<query.lastError().text();
          } else {
              int fieldPing = query.record().indexOf(OC_PING);
              result = query.value(fieldPing).toDateTime();
          }
      }
    return result;
}

bool Azahar::setPing(const qulonglong &order)
{
    bool result=false;
    if (!db.isOpen()) db.open();
      if (db.isOpen()) {
        QSqlQuery query(db);
        QDateTime now = QDateTime::currentDateTime();
        query.prepare(QString("UPDATE %1 SET %2=:ping WHERE %3=:id").arg(ORDERS_TABLE).arg(OC_PING).arg(OC_ID));
        query.bindValue(":id", order);
        query.bindValue(":ping", now);
        if (query.exec()) {
            result = true;
            qDebug()<<"Rows Affected by the ORDER ping UPDATE:"<<query.numRowsAffected()<<" Ping time:"<<now.toString();
        }
        else {
          setError(query.lastError().text());
          qDebug()<<"ERROR: "<<query.lastError();
        }
      }
    return result;
}

//returns orphan orders list, and sets the STATUS_ORPHAN / STATUS_ORPHAN_WARNING according to its ping'ed time
QList<qulonglong> Azahar::getOrphanOrders(const qulonglong &courierId)
{
    QList<qulonglong> result;
    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        if (courierId == 0)
            query.prepare(QString("SELECT * from %1 WHERE %1.%2=%3 OR %1.%2=%4 OR %1.%2=%5 ").arg(ORDERS_TABLE)
                           .arg(OC_STATUSID).arg(STATUS_PICKCONFIRMED).arg(STATUS_ONTHEWAY).arg(STATUS_ORPHAN));
        else {
            query.prepare(QString("SELECT * from %1 where %2=:courier AND (%3=%4 OR %3=%5 OR %3=%6)").arg(ORDERS_TABLE).arg(OC_COURIERID)
                          .arg(OC_STATUSID).arg(STATUS_PICKCONFIRMED).arg(STATUS_ONTHEWAY).arg(STATUS_ORPHAN) );
            query.bindValue(":courier", courierId);
        }
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING ORPHANS ORDERS:"<<query.lastError().text();
              qDebug()<<"Query:"<<query.executedQuery();
          } else {
              while (query.next()) {
                int fieldPing  = query.record().indexOf(OC_PING);
                int fieldId    = query.record().indexOf(OC_ID);
                int fieldStatus= query.record().indexOf(OC_STATUSID);
                int status     = query.value(fieldStatus).toInt();
                qulonglong oid = query.value(fieldId).toULongLong();
                QDateTime  pingDt  = query.value(fieldPing).toDateTime();
                //check if its orphaned
                //qDebug()<<"   //\\ getOrphanOrders()  - Order "<<oid<<" STATUS:"<<status;
                QDateTime now = QDateTime::currentDateTime();
                int elapsedSeconds =  pingDt.secsTo(now);
                if ( elapsedSeconds > 61 &&  elapsedSeconds < 86400) {
                    result.append(oid);
                    setOrderStatus(oid, STATUS_ORPHAN);//only sets the STATUS_ORPHAN to the orders for the courierId with more than 1 minute old ping
                } //if elapsed > 61
                else if ( elapsedSeconds >  86400 ) {
                    result.append(oid);
                    setOrderStatus(oid, STATUS_ORPHAN_WARNING);//set a warning for the very long orphan order (more than 24 hrs)
                }//else if...
              }
          }
      }
    return result;
}

int Azahar::interruptedOrders(const qulonglong &courierId)
{
    int result = 0;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        if (courierId == 0)
            query.prepare(QString("SELECT * from %1 WHERE %2>%3 AND %2<%4 ").arg(ORDERS_TABLE)
                           .arg(OC_STATUSID).arg(STATUS_INTERRUPTED).arg(STATUS_ORPHAN_WARNING));
        else {
            query.prepare(QString("SELECT * from %1 where %2=:courier AND (%3>%4 AND %3<%5)").arg(ORDERS_TABLE).arg(OC_COURIERID)
                          .arg(OC_STATUSID).arg(STATUS_INTERRUPTED).arg(STATUS_ORPHAN_WARNING) );
            query.bindValue(":courier", courierId);
        }
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING ORPHANS ORDERS:"<<query.lastError().text();
          } else {
              result = query.size();
          }
      }

    return result;
}


LCClientInfo Azahar::getClientInfo(qulonglong clientid)
{
    LCClientInfo info;
    info.id = 0;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT * from clients where id=:ID");
        query.bindValue(":ID", clientid);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING CLIENT INFO:"<<query.lastError().text();
          } else {
              //get query result
              while (query.next()) {
                  int fieldFName = query.record().indexOf("firstname");
                  int fieldLName = query.record().indexOf("lastname");
                  int fieldAddr1 = query.record().indexOf("addr1");
                  int fieldAddr2 = query.record().indexOf("addr2");
                  //int fieldComp  = query.record().indexOf("company");
                  int fieldEmail = query.record().indexOf("email");
                  int fieldPass  = query.record().indexOf("password");
                  int fieldPhone = query.record().indexOf("phone");
                  int fieldCity  = query.record().indexOf("city_id"); //as a foreignkey it has appended _id (by django)
                  int fieldState = query.record().indexOf("state");
                  int fieldZip   = query.record().indexOf("zip");
                  int fieldSince = query.record().indexOf("since");
                  int fieldPos   = query.record().indexOf("position");
                  int fieldIP    = query.record().indexOf("lastip");
                  int fieldMailme = query.record().indexOf("emailme");

                  info.firstname = query.value(fieldFName).toString();
                  info.lastname  = query.value(fieldLName).toString();
                  info.addr1     = query.value(fieldAddr1).toString();
                  info.addr2     = query.value(fieldAddr2).toString();
                  //info.company   = query.value(fieldComp).toString();
                  info.email     = query.value(fieldEmail).toString();
                  info.password  = query.value(fieldPass).toString();
                  info.phone     = query.value(fieldPhone).toString();
                  info.zip       = query.value(fieldZip).toString();
                  info.since     = query.value(fieldSince).toDate();
                  info.pos       = query.value(fieldPos).toString();
                  info.lastip    = query.value(fieldIP).toString();
                  info.emailme   = query.value(fieldMailme).toBool();

                  //City, State are on its own table
                  //FIXME:
                  //for now, state is TEXT. Using the USStateField() from django let us selec the state text.
                  qulonglong cityId = query.value(fieldCity).toULongLong();
                  //qulonglong stateId= query.value(fieldState).toULongLong();

                  info.city      =  getCityName(cityId);
                  info.state     =  query.value(fieldState).toString(); //getStateName(stateId);

                  //qDebug()<<"client Name :"<<info.lastname<<", "<<info.firstname;
              }
          }
    }
    return info;
}


QString Azahar::getProviderName(qulonglong id)
{
    QString name;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT name from providers where id=:ID");
        query.bindValue(":ID", id);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING PROVIDER:"<<query.lastError().text();
          } else {
              while (query.next()) {
                int fieldName = query.record().indexOf("name");
                 name = query.value(fieldName).toString();
                //qDebug()<<"PROVIDER:"<<name<<" - "<<id;
            }
        }
    }

    return name;
}

QString Azahar::getProviderPosition(qulonglong id)
{
    QString pos;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT pos from providers where id=:ID");
        query.bindValue(":ID", id);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING PROVIDER:"<<query.lastError().text();
          } else {
              while (query.next()) {
                int fieldPos = query.record().indexOf("pos");
                 pos = query.value(fieldPos).toString();
                //qDebug()<<"PROVIDER Position:"<<pos;
            }
        }
    }

    return pos;
}

LCCourierInfo Azahar::getCourierInfo(qulonglong id)
{
    LCCourierInfo info;
    info.id = 0;
    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT * from couriers where id=:ID");
        query.bindValue(":ID", id);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING COURIER:"<<query.lastError().text();
          } else {
              while (query.next()) {
                int fieldPos  = query.record().indexOf("position");
                int fieldName = query.record().indexOf("name");
                int fieldPhone= query.record().indexOf("phone");
                int fieldKey  = query.record().indexOf("key");
                info.name = query.value(fieldName).toString();
                info.pos  = query.value(fieldPos).toString();
                info.phone= query.value(fieldPhone).toString();
                info.key  = query.value(fieldKey).toString();
                info.id   = id;
            }
        }
    }
    return info;
}

QString Azahar::getCommentsForShipment(qulonglong soid)
{
    QString comments;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT comments from sub_orders where id=:ID");
        query.bindValue(":ID", soid);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING Comment:"<<query.lastError().text();
          } else {
              while (query.next()) { //its supposed to have only one comment per sub_order (restaurant).
                int fieldComment = query.record().indexOf("comments");
                comments = query.value(fieldComment).toString();
                qDebug()<<"COMMENTS FOR THE SHIPPMENT:"<<comments;
            }
        }
    }

    return comments;
}


QList<LCPrintOrderItems> Azahar::getPrintOrderItems(qulonglong soid)
{
    QList<LCPrintOrderItems> orderItems;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT qty,name,suborderid_id from order_items where suborderid_id=:ID");
        query.bindValue(":ID", soid);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING order_items:"<<query.lastError().text();
          } else {
              while (query.next()) {
                LCPrintOrderItems oi;
                int fieldQty  = query.record().indexOf("qty");
                int fieldName = query.record().indexOf("name");
                oi.name = query.value(fieldName).toString();
                oi.qty  = query.value(fieldQty).toDouble();
                orderItems.append(oi);
            }
        }
     }

    return orderItems;
}


QString Azahar::getCityName(qulonglong id)
{
    QString name;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT name from cities where id=:ID");
        query.bindValue(":ID", id);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING CITY:"<<query.lastError().text();
          } else {
              while (query.next()) {
                int fieldName = query.record().indexOf("name");
                name = query.value(fieldName).toString();
                //qDebug()<<"CITY:"<<name<<" - "<<id;
            }
        }
    }

    return name;
}

QString Azahar::getStateName(qulonglong id)
{
    QString name;

    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT name from states where id=:ID");
        query.bindValue(":ID", id);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON STATE CITY:"<<query.lastError().text();
          } else {
              while (query.next()) {
                int fieldName = query.record().indexOf("name");
                name = query.value(fieldName).toString();
                //qDebug()<<"STATE:"<<name<<" - "<<id;
            }
        }
    }

    return name;
}


LCUserInfo Azahar::getUserInfo(const qulonglong &id)
{
    LCUserInfo info;
    info.id = 0;
    if (!db.isOpen()) db.open();
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare(QString("SELECT * FROM %1 WHERE %1.%2=:ID").arg(USR_TABLE).arg(USR_ID));
        query.bindValue(":ID", id);
        if (!query.exec()) {
              setError(query.lastError().text());
              qDebug()<<"ERROR ON GETTING USER INFO:"<<query.lastError().text();
              qDebug()<<"Query Str:"<<query.executedQuery();
          } else {
              while (query.next()) {
                 int fieldSalt  = query.record().indexOf(USR_SALT);
                int fieldName  = query.record().indexOf(USR_NAME);
                int fieldLogin = query.record().indexOf(USR_LASTLOGIN);
                int fieldKey   = query.record().indexOf(USR_KEY);
                info.name = query.value(fieldName).toString();
                info.key  = query.value(fieldKey).toString();
                info.lastLogin = query.value(fieldLogin).toDateTime();
                info.salt = query.value(fieldSalt).toString();
                info.id   = id;
                qDebug()<<" Requested user:"<<id<<info.name;
            }
        }
    }
    return info;
}
