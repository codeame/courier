/***************************************************************************
 *   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
 *   miguel@lemonpos.org                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#include <QApplication>
#include <QLocale>
#include <QDebug>
#include <QTimer>
#include <QTextCodec>
#include <QtSql>
#include <QSettings>
#include <QIntValidator>
#include <QSignalMapper>
#include <QPrinter>
#include <QPrintDialog>

#include "../printing/print-dev.h"
#include "../printing/print-cups.h"
#include "../structs.h"
#include "../azahar.h"
#include "../misc.h"
#include "suborderdelegate.h"

#include "mainwindow.h"
#include "ui_mainwindow.h"

//#include "../mibitWidgets/mibittip.h"
#include "../mibitWidgets/mibitfloatpanel.h"
//#include "../mibitWidgets/mibitdialog.h"
#include "../mibitWidgets/mibitnotifier.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qDebug()<<tr("Starting dispatch...");

    readSettings();

    modelsCreated=false;
    etaValue = 0;
    etaMin = etaHrs = 0;

    //getting the images path.
    QString imgPath = Misc::getAppDataPath()+"images/";
    QString icoPath = Misc::getAppIconPath();

    //set Icon.
    setWindowIcon( QIcon(icoPath+"dispatchApp.png") );

    //Assing controls to panels.
    groupPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top);
    groupPanel->setSize(311,250);
    groupPanel->addWidget(ui->groupConfig);
    groupPanel->setMode(pmManual);
    groupPanel->setHiddenTotally(true);
    groupPanel->hide();

    confirmPanel = new MibitFloatPanel(this, imgPath+"panel_top.svg", Top);
    confirmPanel->setSize(600,550);
    confirmPanel->addWidget(ui->groupConfirm);
    confirmPanel->setMode(pmManual);
    confirmPanel->setHiddenTotally(true);
    confirmPanel->hide();

    readyPanel = new MibitFloatPanel(ui->confirmedView,imgPath+"panel_top.svg", Top );
    readyPanel->setSize(250,150);
    readyPanel->addWidget(ui->groupReady);
    readyPanel->setMode(pmManual);
    readyPanel->setHiddenTotally(true);
    readyPanel->hide();

    shipPanel = new MibitFloatPanel(ui->readyView,imgPath+"panel_top.svg", Top );
    shipPanel->setSize(250,150);
    shipPanel->addWidget(ui->groupPShip);
    shipPanel->setMode(pmManual);
    shipPanel->setHiddenTotally(true);
    shipPanel->hide();

    printPanel = new MibitFloatPanel(this,imgPath+"panel_top.svg", Top );
    printPanel->setSize(270,250);
    printPanel->addWidget(ui->printConfig);
    printPanel->setMode(pmManual);
    printPanel->setHiddenTotally(true);
    printPanel->hide();

    QPixmap iconError = QPixmap(imgPath+"dialog-error.png");
    errorMsg = new MibitNotifier(this,imgPath+"rotated_panel_top.svg", iconError , false);
    errorMsg->setSize(600,100);
    errorMsg->hide();

    //set int validator to hour/minutes
    ui->editEstimatedHours->setValidator(new QIntValidator(0, 99, this));
    ui->editEstimatedMinutes->setValidator(new QIntValidator(0, 999, this));

    connect( ui->btnSaveConfig, SIGNAL(clicked()), groupPanel, SLOT(hidePanel()) );
    connect( ui->btnSaveConfig, SIGNAL(clicked()), this, SLOT(writeSettings()) );
    connect( ui->btnSaveConfig, SIGNAL(clicked()), this, SLOT(settingsDBChanged()));
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), this, SLOT(writeSettings()));
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), printPanel, SLOT(hidePanel()) );
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), this, SLOT(refreshSOFilter()) );
    connect( ui->btnSavePrinterConfig, SIGNAL(clicked()), this, SLOT( updateProviderName() ) );
    connect( ui->actionShow_Settings, SIGNAL(triggered()), this, SLOT(showDBConfig()));
    connect( ui->actionShow_Printer_Settings, SIGNAL(triggered()), this, SLOT(showPrintConfig()));
    connect( ui->actionExit, SIGNAL(triggered()), this, SLOT(quit()));
    connect(ui->unconfirmedView, SIGNAL(clicked(const QModelIndex &)), SLOT(showConfirmation(const QModelIndex &)) );
    connect(ui->btnConfirmCancel, SIGNAL(clicked()), confirmPanel, SLOT(hidePanel()) );
    connect(ui->btnConfirmCancel, SIGNAL(clicked()), this, SLOT(resetETA()) );
    connect(ui->btnConfirm, SIGNAL(clicked()), this, SLOT(confirmOrder()));
    connect(ui->btnReset, SIGNAL(clicked()), this, SLOT(resetETA()));
    connect(ui->btnManual, SIGNAL(toggled(bool)), this, SLOT(manualClicked()));

    connect(ui->confirmedView, SIGNAL(clicked(const QModelIndex &)), SLOT(showReady(const QModelIndex &)) );
    connect(ui->btnSetStatus, SIGNAL(clicked()), this, SLOT(setReady()) );
    connect(ui->btnNo, SIGNAL(clicked()), readyPanel, SLOT(hidePanel()) );

    connect(ui->btnCancelShip, SIGNAL(clicked()), shipPanel, SLOT(hidePanel()) );
    connect(ui->btnShip, SIGNAL(clicked()), this, SLOT(shipIt()) );
    connect(ui->readyView, SIGNAL(clicked(const QModelIndex &)), SLOT(showShip(const QModelIndex &)) );

    //connecting keypad signals
    QSignalMapper *signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(digitClicked(int)));

    signalMapper->setMapping(ui->btn0, 0);
    connect(ui->btn0, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn1, 1);
    connect(ui->btn1, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn2, 2);
    connect(ui->btn2, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn3, 3);
    connect(ui->btn3, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn4, 4);
    connect(ui->btn4, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn5, 5);
    connect(ui->btn5, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn6, 6);
    connect(ui->btn6, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn7, 7);
    connect(ui->btn7, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn8, 8);
    connect(ui->btn8, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn9, 9);
    connect(ui->btn9, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn10, 10);
    connect(ui->btn10, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn15, 15);
    connect(ui->btn15, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn20, 20);
    connect(ui->btn20, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn25, 25);
    connect(ui->btn25, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->btn30, 30);
    connect(ui->btn30, SIGNAL(clicked()), signalMapper, SLOT(map()));

    connect(this, SIGNAL(digitClicked(int)), this, SLOT(numberClicked(int)));

    QTimer::singleShot(1000, this, SLOT(setupDB()));
    QTimer *updateTimer = new QTimer(this);
    updateTimer->setInterval(3000);
    connect(updateTimer, SIGNAL(timeout()), SLOT(refreshModels()) );
    updateTimer->start();

    ui->lblProviderName->setText( tr("Provider #%1").arg( ui->editCfgProviderId->text() ) );

    ui->rbHrs->hide();
    ui->rbMin->hide();

    //setMaximumWidth(800); //NOTE: JUST TESTING. WILL THE SCREEN SIZE BE SMALL?

    statusBar()->showMessage(tr("DispatchApp (C) 2010, Miguel Chavez Gamboa"));
}

void MainWindow::showDBConfig()
{
    //load config
    readSettings();
    //show it...
    groupPanel->showPanel();
    //set focus to the db field
    ui->editCfgDatabase->setFocus();
}

void MainWindow::showPrintConfig()
{
    //load config
    readSettings();
    //show it...
    printPanel->showPanel();
}

void MainWindow::updateProviderName()
{
    if (db.isOpen()) {
        Azahar *myDb = new Azahar();
        myDb->setDatabase(db);
        //get provider name
        QString name = myDb->getProviderName( ui->editCfgProviderId->text().toULongLong() );
        ui->lblProviderName->setText( tr("Provider: %1").arg(name) );
    } else {
        ui->lblProviderName->setText( tr("Provider #%1").arg( ui->editCfgProviderId->text() ) );
    }
}

void MainWindow::readSettings()
 {
     QSettings settings("lemonCourier_dispatch", "lemonPOS Dispatch Mode");
     ui->editCfgDatabase->setText(settings.value("dbName", QString("lemondb")).toString());
     ui->editCfgHost->setText(settings.value("dbHost", QString("localhost")).toString());
     ui->editCfgPassword->setText(settings.value("dbPassword", QString("")).toString());
     ui->editCfgProvider->setCurrentIndex( ui->editCfgProvider->findText( (settings.value("dbProvider", QString("PostgreSQL")).toString()) , Qt::MatchExactly ) );
     ui->editCfgUser->setText(settings.value("dbUser", QString("lemonuser")).toString());

     ui->editCfgDevice->setText(settings.value("printerDevice", QString("/dev/lp0")).toString());
     ui->rbUseCUPS->setChecked( settings.value("printerUseCUPS", true).toBool() );
     ui->rbUseDEV->setChecked( settings.value("printerUseDEV", false).toBool() );

     ui->editCfgProviderId->setText(settings.value("providerID", QString("PLEASE ENTER YOUR ID")).toString());

     restoreGeometry(settings.value("geometry").toByteArray());
     restoreState(settings.value("windowState").toByteArray());
 }

void MainWindow::writeSettings()
 {
     QSettings settings("lemonCourier_dispatch", "lemonPOS Dispatch Mode");
     settings.setValue("dbName", ui->editCfgDatabase->text());
     settings.setValue("dbHost", ui->editCfgHost->text());
     settings.setValue("dbPassword", ui->editCfgPassword->text());
     settings.setValue("dbProvider", ui->editCfgProvider->currentText());
     settings.setValue("dbUser", ui->editCfgUser->text());

     settings.setValue("printerDevice", ui->editCfgDevice->text());
     settings.setValue("printerUseCUPS", ui->rbUseCUPS->isChecked() );
     settings.setValue("printerUseDEV", ui->rbUseDEV->isChecked() );

     settings.setValue("providerID", ui->editCfgProviderId->text().toULongLong());
 }

/* NOTE: Config needs a keyboard!!!.. or use a virtual one.
   Once is configured, its not needed, so its no need to make available the config dialog  */
void MainWindow::settingsDBChanged()
{
  if (db.isOpen()) db.close();

  QString toCompare;
  qDebug()<<" Database STR ON COMBO:"<<ui->editCfgProvider->currentText()<<" Database still in use:"<<db.driverName();
  if (ui->editCfgProvider->currentText() == "MySQL") toCompare = "QMYSQL"; else toCompare = "QPSQL";

  if (toCompare != db.driverName())  {
      qDebug()<<"Database driver was changed...  new driver:"<<db.driverName();
      if (modelsCreated) {
        delete cShipmentsModel;
        delete uShipmentsModel;
        delete orderItemsModel;
        delete readyModel;
      }
      db.close();
      db.removeDatabase(db.connectionName());

      QString dbName = "";
      if (ui->editCfgProvider->currentText() == "MySQL")
	  dbName = "QMYSQL";
      else //for now only mysql and psql
	  dbName = "QPSQL";

      db = QSqlDatabase::addDatabase(dbName);
      qDebug()<<"Database Added: "<<db;

      // QSqlTableModels need to be created after adding the database ( db = addDatabase... )
      cShipmentsModel = new QSqlTableModel();
      uShipmentsModel = new QSqlTableModel();
      orderItemsModel = new QSqlTableModel();
      readyModel      = new QSqlTableModel();
      modelsCreated = true;

      db.setHostName(ui->editCfgHost->text());
      db.setDatabaseName(ui->editCfgDatabase->text());
      db.setUserName(ui->editCfgUser->text());
      db.setPassword(ui->editCfgPassword->text());
      connectToDb();
  } else {
      return;
  }

  //setupModel();
}

void MainWindow::setupDB()
{
  qDebug()<<"Setting up database...";
  if (db.isOpen()) db.close();
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

  QString dbName = "";
  if (ui->editCfgProvider->currentText() == "MySQL")
      dbName = "QMYSQL";
  else //for now only mysql and psql
      dbName = "QPSQL";

  qDebug()<<"DB DRIVERS:"<<db.drivers();
  qDebug()<<"Adding the new Database...";
  db = QSqlDatabase::addDatabase(dbName);
  db.setHostName(ui->editCfgHost->text());
  db.setDatabaseName(ui->editCfgDatabase->text());
  db.setUserName(ui->editCfgUser->text());
  db.setPassword(ui->editCfgPassword->text());

  connectToDb();
}

void MainWindow::connectToDb()
{
  if (!db.open()) {
    db.open(); //try to open connection
    qDebug()<<"(1/connectToDb) Trying to open connection to database..";
  }
  if (!db.isOpen()) {
    db.open(); //try to open connection again...
    qDebug()<<"(2/connectToDb) Trying to open connection to database..";
  }
  if (!db.isOpen()) {
    qDebug()<<"(3/connectToDb) Configuring..";
    groupPanel->showPanel();
  } else {
    qDebug()<<" CONNECTED. ";
    if (!modelsCreated) { //Create models...
      qDebug()<<"Creating model...";
      uShipmentsModel       = new QSqlTableModel();
      cShipmentsModel       = new QSqlTableModel();
      orderItemsModel       = new QSqlTableModel();
      readyModel            = new QSqlTableModel();
      modelsCreated         = true;
    }
    qDebug()<<"Calling setupModel()";
    setupModel();
  }
}


void MainWindow::setupModel()
{
  qDebug()<<"SetupModel...";
  if (!db.isOpen()) {
    connectToDb();
  }
  else {
    qDebug()<<"setting up database model";
    uShipmentsModel->setTable(SUBORDERS_TABLE);
    cShipmentsModel->setTable(SUBORDERS_TABLE); //VIEWC);
    orderItemsModel->setTable(ORDERITEMS_TABLE);
    readyModel->setTable(SUBORDERS_TABLE);//VIEWR);

    if (uShipmentsModel->tableName() != SUBORDERS_TABLE)
      uShipmentsModel->setTable(SUBORDERS_TABLE);
    if (cShipmentsModel->tableName() != SUBORDERS_TABLE) //VIEWC)
      cShipmentsModel->setTable(SUBORDERS_TABLE); //VIEWC);
    if (orderItemsModel->tableName() != ORDERITEMS_TABLE)
      orderItemsModel->setTable(ORDERITEMS_TABLE);
    if (readyModel->tableName() != SUBORDERS_TABLE) //VIEWR)
      readyModel->setTable(SUBORDERS_TABLE);//VIEWR);


    uShipmentsModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    //Remove next, there is no table, its a list.
//    uShipmentsModel->setHeaderData(uShipmentsModel->fieldIndex(SOC_ORDERID), Qt::Horizontal, tr("Order"));
//    uShipmentsModel->setHeaderData(uShipmentsModel->fieldIndex(SOC_LABEL), Qt::Horizontal, tr("Details"));
//    cShipmentsModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
//    cShipmentsModel->setHeaderData(cShipmentsModel->fieldIndex(SOC_ORDERID), Qt::Horizontal, tr("Order"));
//    cShipmentsModel->setHeaderData(cShipmentsModel->fieldIndex(SOC_LABEL), Qt::Horizontal, tr("Details"));
//    orderItemsModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
//    orderItemsModel->setHeaderData(orderItemsModel->fieldIndex(OIC_QTY), Qt::Horizontal, tr("Qty"));
//    orderItemsModel->setHeaderData(orderItemsModel->fieldIndex(OIC_NAME), Qt::Horizontal, tr("Product"));
//    readyModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
//    readyModel->setHeaderData(orderItemsModel->fieldIndex(SOC_ORDERID), Qt::Horizontal, tr("Order"));
//    readyModel->setHeaderData(orderItemsModel->fieldIndex(SOC_LABEL), Qt::Horizontal, tr("Details"));


    ui->unconfirmedView->setModel(uShipmentsModel);
    ui->unconfirmedView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->confirmedView->setModel(cShipmentsModel);
    ui->confirmedView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->orderView->setModel(orderItemsModel);
    ui->orderView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->readyView->setModel(readyModel);
    ui->readyView->setEditTriggers(QAbstractItemView::NoEditTriggers);


    ui->orderView->setColumnHidden(orderItemsModel->fieldIndex(OIC_ORDERID), true);
    ui->orderView->setColumnHidden(orderItemsModel->fieldIndex(OIC_PRODUCTID), true);
    ui->orderView->setColumnHidden(orderItemsModel->fieldIndex(OIC_SUBORDERID), true);
    ui->orderView->setColumnHidden(orderItemsModel->fieldIndex(OIC_PRICE), true);
    ui->orderView->setColumnHidden(orderItemsModel->fieldIndex(OIC_TAX), true);
    ui->orderView->setColumnHidden(orderItemsModel->fieldIndex(OIC_PROVID), true);
    ui->orderView->setColumnHidden(orderItemsModel->fieldIndex(OIC_ID), true);


    SubOrderDelegate *newSOrdersDelegate = new SubOrderDelegate(ui->unconfirmedView, db);
    ui->unconfirmedView->setItemDelegate(newSOrdersDelegate);

    SubOrderDelegate *cSOrdersDelegate = new SubOrderDelegate(ui->confirmedView, db);
    ui->confirmedView->setItemDelegate(cSOrdersDelegate);

    SubOrderDelegate *rSOrdersDelegate = new SubOrderDelegate(ui->readyView, db);
    ui->readyView->setItemDelegate(rSOrdersDelegate);


    uShipmentsModel->setFilter( QString("%1.%2=%5 AND %1.%3=%4")
                                .arg(SUBORDERS_TABLE).arg(SOC_STATUSID)
                                .arg(SOC_PROVID).arg(ui->editCfgProviderId->text()).arg(STATUS_UNCONFIRMED) );
    //PROBLEM: the View conflicts with the DELEGATE!!!!, instead use the normal 'SUBORDER_TABLE' table in the setupDB!
    //cShipmentsModel->setFilter(QString("%1.%2=%3 ")
    //                           .arg(SUBORDERS_VIEWC).arg(SOC_PROVID).arg(ui->editCfgProviderId->text()));
    cShipmentsModel->setFilter( QString("%1.%2=%5 AND %1.%3=%4")
                                .arg(SUBORDERS_TABLE).arg(SOC_STATUSID)
                                .arg(SOC_PROVID).arg(ui->editCfgProviderId->text()).arg(STATUS_CONFIRMED) );
    //readyModel->setFilter(QString("%1.%2=%3 ")
    //                           .arg(SUBORDERS_VIEWR).arg(SOC_PROVID).arg(ui->editCfgProviderId->text()));
    readyModel->setFilter( QString("%1.%3=%4 AND (%1.%2=%5 OR %1.%2=%6)")
                                .arg(SUBORDERS_TABLE).arg(SOC_STATUSID)
                                .arg(SOC_PROVID).arg(ui->editCfgProviderId->text()).arg(STATUS_READY).arg(STATUS_PICKCONFIRMED) );

    orderItemsModel->setFilter("1=1"); //the filter is set when a row is selected

    refreshModels();
    //ui->orderView->resizeColumnsToContents();

    updateProviderName();

    QTimer::singleShot(200,this, SLOT(adjustTables()));

  }
}

//this is needed to update the provider id.
void MainWindow::refreshSOFilter()
{
    if (modelsCreated && db.isOpen()) {
        uShipmentsModel->setFilter( QString("%1.%2=%5 AND %1.%3=%4")
                                    .arg(SUBORDERS_TABLE).arg(SOC_STATUSID)
                                    .arg(SOC_PROVID).arg(ui->editCfgProviderId->text()).arg(STATUS_UNCONFIRMED) );

        //PROBLEM: the View conflicts with the DELEGATE!!!!, instead use the normal 'SUBORDER_TABLE' table in the setupDB!
        //cShipmentsModel->setFilter(QString("%1.%2=%3 ")
        //                           .arg(SUBORDERS_TABLE).arg(SOC_PROVID).arg(ui->editCfgProviderId->text()));
        cShipmentsModel->setFilter( QString("%1.%2=%5 AND %1.%3=%4")
                                    .arg(SUBORDERS_TABLE).arg(SOC_STATUSID)
                                    .arg(SOC_PROVID).arg(ui->editCfgProviderId->text()).arg(STATUS_CONFIRMED) );

        //readyModel->setFilter(QString("%1.%2=%3 ")
        //                           .arg(SUBORDERS_VIEWR).arg(SOC_PROVID).arg(ui->editCfgProviderId->text()));
        readyModel->setFilter( QString("%1.%3=%4 AND (%1.%2=%5 OR %1.%2=%6)")
                                    .arg(SUBORDERS_TABLE).arg(SOC_STATUSID)
                                    .arg(SOC_PROVID).arg(ui->editCfgProviderId->text()).arg(STATUS_READY).arg(STATUS_PICKCONFIRMED) );
        refreshModels();
    }
}

void MainWindow::refreshModels()
{
    if ( modelsCreated && db.isOpen() ) {
       uShipmentsModel->select();
       cShipmentsModel->select();
       orderItemsModel->select();
       readyModel->select();
    }
}


void MainWindow::adjustTables()
{
  if (modelsCreated) {
      QSize size = ui->orderView->size();
      int portion = size.width()/5;
      ui->orderView->horizontalHeader()->setResizeMode(QHeaderView::Interactive);
      ui->orderView->horizontalHeader()->resizeSection(orderItemsModel->fieldIndex(OIC_QTY), portion*1.4); // Qty
      ui->orderView->horizontalHeader()->resizeSection(orderItemsModel->fieldIndex(OIC_NAME), portion*3.4); //Name
  }
}

void MainWindow::showConfirmation(const QModelIndex &index)
{
    //clear edits
    ui->editEstimatedHours->clear();
    ui->editEstimatedMinutes->clear();
    //get all needed data...
    qulonglong row = index.row();
    QModelIndex indx = uShipmentsModel->index(row, uShipmentsModel->fieldIndex(SOC_ORDERID));
    orderId = uShipmentsModel->data(indx, Qt::DisplayRole).toULongLong();
    indx =  uShipmentsModel->index(row, uShipmentsModel->fieldIndex(SOC_ID));
    soId = uShipmentsModel->data(indx, Qt::DisplayRole).toULongLong();
    orderItemsModel->setFilter(QString("%1.%2=%3 AND %1.%4=%5").arg(ORDERITEMS_TABLE).arg(OIC_ORDERID)
                               .arg(orderId).arg(OIC_PROVID).arg(ui->editCfgProviderId->text()));
    orderItemsModel->select();
    //show the dialog
    //confirmDialog->showDialog();
    confirmPanel->showPanel();
    adjustTables();
    //disable confirm button
    ui->btnConfirm->setDisabled(true);
}

void MainWindow::showReady(const QModelIndex &index)
{
    qulonglong row = index.row();
    QModelIndex indx = cShipmentsModel->index(row, cShipmentsModel->fieldIndex(SOC_ORDERID));
    orderId = cShipmentsModel->data(indx, Qt::DisplayRole).toULongLong();
    indx    = cShipmentsModel->index(row, cShipmentsModel->fieldIndex(SOC_ID));
    soId    = cShipmentsModel->data(indx, Qt::DisplayRole).toULongLong();
    indx    = cShipmentsModel->index(row, cShipmentsModel->fieldIndex(SOC_LABEL));
    QString orderLabel = cShipmentsModel->data(indx, Qt::DisplayRole).toString();
    ui->lblStatus->setText(QString("The order %1 is ready to pick up?").arg(orderLabel));
    readyPanel->showPanel();
    //orderId is set to the selected order.
}

void MainWindow::showShip(const QModelIndex &index)
{
    qulonglong row = index.row();
    QModelIndex indx = readyModel->index(row, readyModel->fieldIndex(SOC_ORDERID));
    orderId = readyModel->data(indx, Qt::DisplayRole).toULongLong();
    indx    = readyModel->index(row, readyModel->fieldIndex(SOC_ID));
    soId    = readyModel->data(indx, Qt::DisplayRole).toULongLong();
    indx    = readyModel->index(row, readyModel->fieldIndex(SOC_LABEL));
    QString orderLabel = readyModel->data(indx, Qt::DisplayRole).toString();
    ui->lblShip->setText(QString("The order %1 is being picked up?").arg(orderLabel));
    shipPanel->showPanel();
    //orderId is set to the selected order. for the shipIt() method
}

void MainWindow::confirmOrder()
{
    //get ETA from the dialog. sum minutes + hours*60
    //TODO: when improved the key pad, i added a variable for estimated_minutes... check that!
    int minutes = ui->editEstimatedMinutes->text().toInt() + ui->editEstimatedHours->text().toInt()*60;

    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    bool ok = false;
    ok = myDb->setSubOrderStatus( soId, STATUS_CONFIRMED );
    if (ok) { //status will be a key of OrderStatus table, take care of this.
        ok = myDb->setSubOrderETA( soId, minutes );
        if (!ok) displayError(tr("Could not execute the database modification:\n%1").arg(myDb->lastError()));
        else qDebug()<<" +++ SUBORDER #"<<soId<<" Changed to STATUS_CONFIRMED";
    } else {
        displayError(tr("Could not execute the database modification:\n%1").arg(myDb->lastError()));
    }
    if (ok) printKitchenOrder(orderId); //TODO: make something to re-try.
    delete myDb;

    //finally close the panel
    confirmPanel->hidePanel();
    refreshModels();
    orderId = 0; //reset
    resetETA();
}

void MainWindow::setReady()
{
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);

    if (!myDb->setSubOrderStatus( soId, STATUS_READY ))
        displayError(tr("Could not execute the database modification:\n%1").arg(myDb->lastError()));
    else qDebug()<<" +++ SUBORDER #"<<soId<<" Changed to STATUS_READY";
    delete myDb;

    readyPanel->hidePanel();
    refreshModels();
    orderId = 0; //reset.
}

void MainWindow::shipIt()
{
    //mark as shipped.
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);

    if (!myDb->setSubOrderStatus( soId, STATUS_ONTHEWAY ))
        displayError(tr("Could not execute the database modification:\n%1").arg(myDb->lastError()));
    else qDebug()<<" +++ SUBORDER #"<<soId<<" Changed to STATUS_ONTHEWAY";
    delete myDb;

    //finally close the panel
    shipPanel->hidePanel();
    refreshModels();
    orderId = 0; //reset
}


void MainWindow::printKitchenOrder(const qulonglong &orderid)
{
    printOrderInfo(orderid, ui->editCfgProviderId->text().toULongLong());
}


void MainWindow::printOrderInfo(const qulonglong &orderid, const qulonglong &provid)
{
    LCPrintOrderInfo order;
    Azahar *myDb = new Azahar(this);
    myDb->setDatabase(db);

    order = myDb->getPrintOrderInfo(orderid);
    QStringList items; //for matrix printers, simple text
    foreach (LCSubOrderInfo soi ,order.subordersList) {
        if (provid == 0 || provid == soi.providerid) {
            QString provname =  myDb->getProviderName(soi.providerid); //get provider name
            items<<provname;
            //get each item from each suborder
            foreach (LCPrintOrderItems itm, soi.items) {
                QString str = "   "+QString::number(itm.qty)+" x "+itm.name;
                items << str;
            }//each item for each suborder
            //Print COMMENTS.
            items<<tr("\nCOMMENTS:-------------------------------");
            QStringList fixedComments = Misc::stringToParagraph(soi.comments, 40);
            items<<fixedComments.join("\n");
        } //if providerid
    } //each suborder

    //get order information from database...
    if (ui->rbUseDEV->isChecked()) {
        //its a /dev printer...
        QStringList lines;
        lines << tr("Order No. ") + QString::number(orderid);
        lines << QDateTime::currentDateTime().toString("ddd MMM d yyyy, h:mm ap");
        lines << order.clientName;
        lines << order.clientAddr;
        lines << "========================================";
        lines << items.join("\n");
        lines << "========================================";
        //finally print lines
        qDebug()<<lines.join("\n");
        PrintDEV::printSmallTicket(ui->editCfgDevice->text(), "UTF-8", lines.join("\n"));
    } else {
        //its a CUPS printer...
        foreach (LCSubOrderInfo soi, order.subordersList) {
            if (provid == 0 || provid == soi.providerid) {
                QPrinter printer;
                printer.setFullPage( true );
                QPrintDialog printDialog( &printer );
                printDialog.setWindowTitle(tr("Print Order"));
                if ( printDialog.exec() )
                    PrintCUPS::printSubOrder(soi, /*provid,*/ printer);
            }
        }
    }
    delete myDb;
}

void MainWindow::enableConfirm()
{
    if ( etaValue > 0 )
        ui->btnConfirm->setEnabled(true);
    else
        ui->btnConfirm->setEnabled(false);
}

void MainWindow::numberClicked(int n)
{
    //Check the edit mode.
    if (ui->btnAdd->isChecked())
        etaValue += n; // this approach sums the pressed numbers...
    else if (ui->btnSub->isChecked())
        etaValue -= n; // this approach substracts the pressed numbers...
    else {
        // set the number on the selected field, and calculate the etaValue
        if (ui->rbHrs->isChecked()) {
            //add hours
            etaValue = etaValue - (etaHrs*60) + n*60;
        }
        else { //if (ui->editEstimatedMinutes->hasFocus()) {
            //add minutes
            etaValue = etaValue - (etaMin) + n;
        }
    }

    etaMin = 0; etaHrs =0; //reset
    if (etaValue > 59 ) {
        etaHrs = etaValue / 60;
        etaMin = etaValue % 60;
    } else etaMin = etaValue;

    if (etaValue < 0 ) {
        etaValue = etaMin = etaHrs = 0;
        ui->btnAdd->setChecked(true); // set the ADD button enable, cos we are getting negative numbers.
    }

    qDebug()<<" ETA Value:"<<etaValue<<" ETA:"<<etaHrs<<":"<<etaMin;
    ui->editEstimatedHours->setText(QString::number(etaHrs));
    ui->editEstimatedMinutes->setText(QString::number(etaMin));
    enableConfirm();
}

void MainWindow::resetETA()
{
    etaValue = etaMin = etaHrs = 0;
}

void MainWindow::manualClicked()
{
    if (ui->btnManual->isChecked()) {
        ui->rbHrs->show();
        ui->rbMin->show();
    } else {
        ui->rbHrs->hide();
        ui->rbMin->hide();
    }
}

void MainWindow::displayError(const QString &msg)
{
    //launch the panel.
    errorMsg->setTextColor("#ff5500");
    errorMsg->showNotification(msg);
}

void MainWindow::quit()
{
    qApp->exit();
}

void MainWindow::closeEvent(QCloseEvent *event)
 {
     QSettings settings("lemonCourier_dispatch", "lemonPOS Dispatch Mode");
     settings.setValue("geometry", saveGeometry());
     settings.setValue("windowState", saveState());
     QMainWindow::closeEvent(event);
 }

MainWindow::~MainWindow()
{
    delete ui;
    delete confirmPanel;
    delete groupPanel;
    delete readyPanel;
    delete printPanel;
    delete shipPanel;
    if (modelsCreated) {
        delete cShipmentsModel;
        delete uShipmentsModel;
        delete orderItemsModel;
        delete readyModel;
    }
}
