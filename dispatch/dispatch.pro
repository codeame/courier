#-------------------------------------------------
#
# Project created by QtCreator 2010-08-19T19:32:11
#
#-------------------------------------------------

QT       += core gui svg sql

#LIBS += lpq

TARGET = dispatchApp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        ../mibitWidgets/mibitfloatpanel.cpp \
        ../mibitWidgets/mibitnotifier.cpp \
        ../printing/print-dev.cpp \
        ../printing/print-cups.cpp \
        ../azahar.cpp \
        ../misc.cpp \
        suborderdelegate.cpp

HEADERS  += mainwindow.h \
         ../mibitWidgets/mibitfloatpanel.h \
         ../mibitWidgets/mibitnotifier.h \
         ../structs.h \
         ../printing/print-dev.h \
         ../printing/print-cups.h \
         ../azahar.h \
         ../misc.h \
         suborderdelegate.h

FORMS    += mainwindow.ui


TRANSLATIONS    = dispatchApp_es.ts \


#target.path = $$[QT_INSTALL_BIN]

#INSTALLS += target
