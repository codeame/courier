/**************************************************************************
*   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
*   miguel@lemonpos.org                                                   *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
***************************************************************************/

#define QT_USE_FAST_CONCATENATION
#define QT_USE_FAST_OPERATOR_PLUS

#ifndef SUBORDERDELEGATE_H
#define SUBORDERDELEGATE_H

#include <QItemDelegate>
#include <QSqlDatabase>

class QMouseEvent;
class QPainEvent;


class SubOrderDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit SubOrderDelegate(QWidget *parent, const QSqlDatabase& database) : QItemDelegate(parent) { db = database; }
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
protected:
  void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
  QSqlDatabase db;
};

#endif // SUBORDERDELEGATE_H
