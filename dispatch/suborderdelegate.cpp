/**************************************************************************
*   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
*   miguel@lemonpos.org                                                   *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
***************************************************************************/
#include <QtGui>
#include <QtSql>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QLocale>

//#include <valgrind/callgrind.h>

#include "suborderdelegate.h"
#include "../azahar.h"
#include "../structs.h"

QSize SubOrderDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  if(option.rect.width() == 0 || option.rect.height() == 0) return QSize(0, 0);
  QFontMetrics fontMetrics = option.fontMetrics;
  //QRect rect = fontMetrics.boundingRect(index.data().toString());
  //float f = rect.width() / option.rect.width();
  //int approxLineCount = int(f) + 1;
  //return QSize(option.rect.width(), approxLineCount * fontMetrics.height());

  //We will return the rect width but with 6 lines in height.
  return QSize(option.rect.width(), 6*fontMetrics.height());
}

void SubOrderDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (option.state & QStyle::State_Selected)
                 painter->fillRect(option.rect, option.palette.highlight());

   // CALLGRIND_START_INSTRUMENTATION;
    //get item data
    const QAbstractItemModel *model = index.model();
    int row = index.row();
    QModelIndex indx = model->index(row, 0);
    qulonglong  suborderId = model->data(indx, Qt::DisplayRole).toULongLong();

    LCSubOrderInfo soInfo;
    Azahar *myDb = new Azahar();
    myDb->setDatabase(db);
    soInfo = myDb->getPrintSubOrderInfo(suborderId);

    //Painting OrderNo,ClientName.
    QFont font = QApplication::font(); //QFont("Ubuntu", 9);
    font.setPointSize(9);
    font.setBold(true);
    painter->setFont(font);
    painter->setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    QFontMetrics fm(font);
    QString text = soInfo.label;
    painter->drawText(option.rect.x()+2,option.rect.y()+fm.height() , text); //at line 1
    //iterate each order item.
    int i = 0;
    font.setItalic(true);
    font.setBold(false);
    font.setPointSize(9);
    painter->setPen(QPen(Qt::darkGray, 1, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    painter->setFont(font);
    foreach(LCPrintOrderItems item, soInfo.items ) {
        text = QString("   %1 x %2 ").arg( item.qty ).arg( item.name );
        painter->drawText(option.rect.x()+2,option.rect.y()+fm.height()*(2+i) , text); //at line 2,3..
        i++;
    }

    delete myDb;
    //CALLGRIND_STOP_INSTRUMENTATION;
}
