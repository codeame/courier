/***************************************************************************
 *   Copyright (C) 2010 by Miguel Chavez Gamboa                            *
 *   miguel@lemonpos.org                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define QT_USE_FAST_CONCATENATION
#define QT_USE_FAST_OPERATOR_PLUS

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlTableModel>

class MibitFloatPanel;
class MibitNotifier;


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    ///Shows configuration panel
    void showDBConfig();
    void showPrintConfig();
    ///Saves/Reads configuration information.
    void writeSettings();
    void readSettings();
    void adjustTables();
    void showConfirmation(const QModelIndex &index);
    void showShip(const QModelIndex &index);
    void confirmOrder();
    void shipIt();
    void setReady();
    void printKitchenOrder(const qulonglong &orderid);
    //this prints only the order_items for the selected provider. if provid=0, it prints all the items
    void printOrderInfo(const qulonglong &oid, const qulonglong &provid=0);
    //quit
    void quit();
    //React when settings changed
    void settingsDBChanged();
    void setupDB();
    void connectToDb();
    void refreshModels();
    void enableConfirm();
    void numberClicked(int);
    void resetETA();
    void manualClicked();
    void showReady(const QModelIndex &index);
    void refreshSOFilter();
    void updateProviderName();
    void displayError(const QString &msg);

    //each list on view methods
    //void listViewOnClick( const QModelIndex & index );

    //Save an action log.. each processed order,etc..
    //void log(const qulonglong &uid, const QDate &date, const QTime &time, const QString &text);

private:
    Ui::MainWindow *ui;
    MibitFloatPanel *groupPanel, *confirmPanel, *readyPanel, *printPanel, *shipPanel;
    MibitNotifier  *errorMsg;
    QSqlDatabase db;
    QSqlTableModel *cShipmentsModel, *uShipmentsModel, *orderItemsModel, *readyModel;
    bool   modelsCreated;
    qulonglong orderId;
    qulonglong soId;
    int etaValue;
    int etaMin, etaHrs;


    void setupModel();

protected:
     void closeEvent(QCloseEvent *event);

 signals:
    void digitClicked(int);

};

#endif // MAINWINDOW_H
